/** @file abc2html.c
 *
 *  abc music notation software
 *
 *  @par Copyright:
 *            2024-2025 (C) Kai-Uwe Behrmann
 *
 *  @brief    abc2html command line
 *  @internal
 *  @author   Kai-Uwe Behrmann <ku.b@gmx.de>
 *  @par License:
 *            AGPL-3.0 <https://opensource.org/licenses/AGPL-3.0>
 *  @since    2016/12/17
 */
// cc -Wall -Wextra -O0 -g abc2html.c `pkg-config --libs-only-L --cflags oyjl` -o abc2html -loyjl-core-static && ./abc2html -l=header=T,R=Jig,C,X -vi session.abc
#ifdef INCLUDE_OYJL_C
# include "extras/oyjl_args.c"
#else
# include <oyjl.h>
# include <oyjl_macros.h>
# include <oyjl_version.h>
#endif
#ifdef HAVE_MUSICABC
# include <musicabc.h>
# define LIBMUSICABC "libMusicAbc"
#else
# define LIBMUSICABC
#endif
extern char **environ;
#ifdef OYJL_HAVE_LOCALE_H
# include <locale.h>
#endif
#include <string.h> // strlen()
#include <ctype.h> // isspace()
#include <unistd.h> // getcwd()
#define MY_DOMAIN "abc2html"
oyjlTranslation_s * trc = NULL;
#ifdef INCLUDE_OYJL_C
# ifdef _
#  undef _
# endif
# define _(text) text
# define oyjlUi_ToText oyjlUi_ToTextArgsBase
# define oyjlUi_Release oyjlUi_ReleaseArgs
#else
# ifdef _
#  undef _
# endif
# define _(text) oyjlTranslate( trc, text )
#endif

#include "abc2html-data.c"
#include "abc2html_git_version.h"
#include "abc2html_version.h"
#ifndef ABC2HTML_VERSION_NAME
# define ABC2HTML_VERSION_NAME "0.5"
#endif

#define ABC2HTML_ABC        0x01
#define ABC2HTML_HTML       0x02
#define ABC2HTML_SVG        0x04
#define ABC2HTML_PNG        0x08
#define ABC2HTML_PDF        0x10
#define ABC2HTML_MULTIPAGE  0x20
#define ABC2HTML_GRAFIC     0x40

#define ABC2HTML_FIRST_LYRIC_ONLY 0x0080
#define ABC2HTML_BORDERLESS       0x0100
#define ABC2HTML_REMOVE_M_EXTRA   0x0400
#define ABC2HTML_REMOVE_EXTRA     0x0800
#define ABC2HTML_REMOVE_M_HTML    0x1000
#define ABC2HTML_CLEAN_ABC        0x2000

#define ABC2PDF_KEEP_INTERMEDIATE 0x10000

#define ABC2HTML_VERBOSE          0x20000
#define ABC2HTML_VERBOSE2         0x40000
#define ABC2HTML_VERBOSE3         0x80000

#define ABC2HTML_TUNES            0x100000
#define ABC2HTML_ABC_INTRO        0x200000
#define ABC2HTML_ABC_FOOTER       0x400000

const char * abc2htmlStringFind( const char * a, const char * b, const char * c, const char * pattern, int flags )
{
  const char * text = NULL;
  if( oyjlStringFind( a, pattern, flags ) == 1 )
    text = strstr(a, pattern);
  if( !text && oyjlStringFind( b, pattern, flags ) == 1 )
    text = strstr(b, pattern);
  if( !text && oyjlStringFind( c, pattern, flags ) == 1 )
    text = strstr(c, pattern);
  return text;
}

char * abc2htmlBas64Encode( const unsigned char * data, int size, int verbose )
{
  const char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                       "abcdefghijklmnopqrstuvwxyz"
                       "0123456789+/=";
  char * encoded = calloc(size*2+4, sizeof(char));
  unsigned long ul;
  int i, pos;
  for(i = 0; i < size; i+=3)
  {
    unsigned char  triple[3] = {0,0,0};
                   triple[0] = data[i];
    if(i+1 < size) triple[1] = data[i+1];
    if(i+2 < size) triple[2] = data[i+2];
    ul = (unsigned long)triple[0]<<16 | (unsigned long)triple[1]<<8 | (unsigned long)triple[2];
    pos = i/3*4;
    if(verbose >= 2 && i < 10)
      fprintf( stderr, "i[%d] ul:%lu encoded[%d]=%c ", i, ul, pos, table[ul>>18] );
    encoded[pos+0] = table[ul>>18];
    encoded[pos+1] = table[ul>>12 & 63];
    encoded[pos+2] = i+1 >= size ? '=' : table[ul>>6 & 63];
    encoded[pos+3] = i+2 >= size ? '=' : table[ul & 63];
  }
  return encoded;
}

int abc2htmlSizetoPixel( const char * size, double dpi, int verbose )
{
  int pixel = 0;
  const char * end = NULL;
  long l = 0;
  if(size && oyjlStringToLong( size, &l, &end ))
  {
    pixel = l;
    if(end)
    {
      if(oyjlStringFind( end, "cm", OYJL_COMPARE_STARTS_WITH | OYJL_COMPARE_CASE ) > 0)
        pixel = (int)((double) l / 2.54 * dpi);
      else
      if(oyjlStringFind( end, "in", OYJL_COMPARE_STARTS_WITH | OYJL_COMPARE_CASE ) > 0)
        pixel = (int)((double) l * dpi);
    }
    if(verbose)
      fprintf(stderr, OYJL_DBG_FORMAT "Found %s -> %d pixel\n", OYJL_DBG_ARGS, size, pixel );
  }

  return pixel;
}

int abc2htmlIsAbc( const char * abc )
{ return abc && oyjlStringFind( abc, "\nk:", OYJL_COMPARE_LAZY ) ? 1 : 0; }

char * abc2htmlGetAbcField( const char * abc, const char * field_name, int pos )
{
  char * field = NULL, * t2, * search = NULL;
  const char * t;
  int n = 0;
  if(!abc) return field;
  oyjlStringAdd( &search, 0,0, "\n%s", field_name );
  t = abc;
  while((t = strstr( t, search )) != NULL)
  {
    t++;
    if(pos == n++)
      break;
  }
  if(t)
  {
    field = oyjlStringCopy( t, 0 );
    t2 = strchr( field, '\n' );
    if(t2) t2[0] = '\000';
  }
  free(search);
  return field;
}

int abc2htmlFilterAbcs( char ** abcs, int count, const char * filters, int select_i, int*indexes, int verbose )
{
  int i, j, pos = 0, found;
  if(filters || select_i >= 0)
  {
    const char * abc;
    char * t = NULL, * t2 = NULL;
    char * h = NULL;
    char * s = NULL;
    int header = oyjlStringSplitFind(filters, ":", "header", OYJL_COMPARE_STARTS_WITH, &h, 0,0) >= 0;
    int has_filter = h && strlen(h) > 7 && strchr(&h[7], '=') != NULL ? 1 : 0;
    int select = oyjlStringSplitFind(filters, ":", "select", OYJL_COMPARE_STARTS_WITH, &s, 0,0) >= 0;
    int has_select = s && strlen(s) > 7 && oyjlDataFormat( &s[7] ) == 7 ? 1 : 0;
    int header_tags_n = 0;
    char ** header_tags = oyjlStringSplit2( h && strlen(h) > 7 ? &h[7] : h, ",", NULL, &header_tags_n, NULL, NULL );
    if(verbose)
      for(i = 0; i < header_tags_n; ++i)
        fprintf( stderr, OYJL_DBG_FORMAT "[%d]: %s\n", OYJL_DBG_ARGS, i, header_tags[i] );
    if(header)
    {
      if(has_filter)
      for(i = 0; i < count; ++i)
      {
        int lines_n = 0;
        char ** lines;
        found = 1;
        abc = abcs[i];
        lines = oyjlStringSplit2( abc, "\n", NULL, &lines_n, NULL, NULL );
        for(j = 0; j < header_tags_n; ++j)
        {
          const char * ht = header_tags[j];
          char * tag = oyjlStringCopy( ht, 0 ),
               * start = NULL,
               * filter = NULL;
          int line_pos;
          char * line = NULL;
          t = strchr(tag,'=');
          if(t != NULL)
          {
            filter = oyjlStringCopy( t+1, 0 );
            t[0] = '\000';
          }
          if(filter && filter[0] == '\000')
          {
            free(filter); filter = NULL;
          }
          if(filter)
          {
            oyjlStringAdd( &start, 0,0, "%s:", tag );
            line_pos = oyjlStringListFind(lines, &lines_n, start, OYJL_COMPARE_STARTS_WITH, 0);
            if(line_pos >= 0)
            {
              line = lines[line_pos];
              if(verbose)
                fprintf( stderr, OYJL_DBG_FORMAT "found %d %s  %s", OYJL_DBG_ARGS, i, filter, line );
            } else
            {
              found = 0;
              if(verbose)
                fprintf( stderr, OYJL_DBG_FORMAT "%d %s %s", OYJL_DBG_ARGS, i, start, oyjlTermColorF( oyjlRED, "%s", _("not available") ));
            }
            if(line && oyjlStringFind( line, filter, OYJL_COMPARE_LAZY ) <= 0)
            {
              found = 0;
              if(verbose)
                fprintf( stderr, " %s", oyjlTermColor( oyjlRED, _("decline") ));
            }
            if(verbose)
              fprintf( stderr, "%s\n", found?oyjlTermColorF( oyjlGREEN, " %s", _("yes") ):"" );
            if(!found) break;
          }
          if(tag) free(tag);
          if(start) free(start);
          if(filter) free(filter);
        }

        if(found)
          indexes[pos++] = i;

        oyjlStringListRelease( &lines, lines_n, free );
      }
    }

    if(select || select_i >= 0.0)
    {
      int status = 0, n = 0,j;
      oyjl_val root = NULL;
      char * sel = has_select ? &s[7] : s;
      t = t2 = NULL;

      for(i = 0; i < count; ++i)
        indexes[i] = 0;

      if(has_select)
        root = oyjlTreeParse2( sel, 0, __func__, &status );
      else if(select_i < 0)
      {
        fprintf(stderr, OYJL_DBG_FORMAT "%s\t\%s: \"%s\"\n", OYJL_DBG_ARGS, oyjlTermColorF(oyjlRED, "%s", _("Error")), oyjlTermColorPtr(oyjlBOLD, &t, _("Need format \"select=[1,2]\"")), oyjlTermColorPtr(oyjlRED, &t2, s));
        free(t); free(t2); t = t2 = NULL;
      }
      if(status)
      {
        fprintf(stderr, OYJL_DBG_FORMAT "%s\t\"%s\": \"%s\" %s\n", OYJL_DBG_ARGS, oyjlTermColorF(oyjlRED, "%s", _("Error")), oyjlPARSE_STATE_eToString(status), oyjlTermColorPtr(oyjlRED, &t2, s), oyjlTermColorPtr(oyjlBOLD, &t, _("Need format \"select=[1,2]\"")));
        free(t); t = NULL;
        goto clean_abc2htmlFilterAbcs;
      }
      if(select_i >= 0.0)
        n = 1;
      else
        n = oyjlValueCount( root );
      if(!n)
        fprintf(stderr, OYJL_DBG_FORMAT "%s\t\"%s\" %s: %d\n", OYJL_DBG_ARGS, oyjlTermColorF(oyjlRED, "%s", _("Error")), oyjlTermColorPtr(oyjlRED, &t2, s?s:"--select"), _("count"), n);
      else if(verbose)
        fprintf(stderr, OYJL_DBG_FORMAT "%s\t\"%s\": %s: %d\n", OYJL_DBG_ARGS, oyjlTermColorF(oyjlGREEN, "%s", _("Select")), s?s:"--select", _("count"), n);
      for(i = 0; i < n; ++i)
      {
        oyjl_val v = oyjlValuePosGet( root, i );
        int index = select_i>=0.0 ? select_i : OYJL_IS_INTEGER(v) ? OYJL_GET_INTEGER(v) : -1;
        const char * title = OYJL_GET_STRING(v);
        if(index >= 0 && index < count)
        {
          indexes[pos] = index;
          if(verbose)
          {
            fprintf(stderr, OYJL_DBG_FORMAT "%s\t\"%s\": [%d] = %s\n", OYJL_DBG_ARGS, oyjlTermColorF(oyjlGREEN, "%s", _("Select")), s?s:"--select", i, oyjlTermColorFPtr( oyjlGREEN, &t, "%d", index ));
            free(t); t = NULL;
          }
          ++pos;
        }
        else if(title)
        {
          for(j = 0; j < count; ++j)
          {
            char * T;
            abc = abcs[j];
            T = abc2htmlGetAbcField( abc, "T:", 0 );
            if(T && oyjlStringFind(T+2, title, OYJL_COMPARE_LAZY))
            {
              indexes[pos] = j;
              if(verbose)
              {
                fprintf(stderr, OYJL_DBG_FORMAT "%s\t\"%s\": [%d] = %s\n", OYJL_DBG_ARGS, oyjlTermColorF(oyjlGREEN, "%s", _("Select")), title, i, oyjlTermColorFPtr( oyjlGREEN, &t, "%d", j ));
                free(t); t = NULL;
              }
              ++pos;
            }
          }
        }
        else
        {
          fprintf( stderr, "%s: [%d] = %s %s %s: %d\n", oyjlTermColorF(oyjlRED, "%s", _("Ignoring")), i, oyjlTermColorFPtr( oyjlRED, &t, "%d", index), s?s:"", _("count"), count );
          free(t); t = NULL;
        }
      }
      if(root) oyjlTreeFree( root );
    }
    
clean_abc2htmlFilterAbcs:
    oyjlStringListRelease( &header_tags, header_tags_n, free );
    if(h) free(h);
    if(s) free(s);
  }
  return pos;
}

/* return only with flags ABC2HTML_CLEAN_ABC. The ABC will start with returned abc_start line and end with abc_end. */
char * abc2htmlGetAbcRange( const char * abc, int * abc_start_, int * abc_end_, int flags )
{
  int abc_start = *abc_start_,
      abc_end   = *abc_end_,
      verbose = flags & ABC2HTML_VERBOSE;
  char * new_abc = NULL;

  while( abc && abc[0] && isspace(abc[0]) ) abc++;
  if(abc2htmlIsAbc(abc))
  {
    int lines_n = 0, j;
    char ** lines;
    char * line, * text, c;

    lines = oyjlStringSplit2( abc, "\n", NULL, &lines_n, NULL, NULL );
    abc_end = lines_n;

    if( !(flags & ABC2HTML_REMOVE_M_EXTRA) )
    for(j = 0; j < lines_n; ++j)
    {
      line = lines[j];
      text = line;
      while( text && text[0] && isspace(text[0]) ) text++;
      c = text[0];
      if(verbose)
        fprintf( stderr, "[%d]:%s ", j, text );
      if( oyjlStringFind( line, "abc-automatic", OYJL_COMPARE_LAZY ) && oyjlStringFind( line, "end", OYJL_COMPARE_LAZY ) )
        abc_start = j+1;
      if( c == '%' || c == '\n' || c == '\000' )
        continue;
      if(abc_start == 0)
        abc_start = j;
      break;
    }

    for(j = abc_start; j < lines_n; ++j)
    {
      line = lines[j];
      text = line;
      while( text && text[0] && isspace(text[0]) ) text++;
      c = text[0];
      if(c == '\000')
      {
        abc_end = j;
        break;
      }
      if( strstr(text, "</desc>") != NULL || /* SVG */
          strstr(text, "</div>") != NULL )   /* HTML */
      {
        abc_end = j;
        break;
      }
    }

    if(verbose > 1)
      fprintf( stderr, OYJL_DBG_FORMAT "%% -------------%d -> %d ----------------- %d %d\n", OYJL_DBG_ARGS, abc_start, abc_end, abc_end-1, abc_start );

    if( !(flags & ABC2HTML_REMOVE_M_EXTRA || flags & ABC2HTML_REMOVE_M_HTML) )
    for(j = abc_end - 1; j >= abc_start; --j)
    {
      line = lines[j];
      text = line;
      while( text && text[0] && isspace(text[0]) ) text++;
      c = text[0];
      if(verbose)
        fprintf( stderr, "[%d]:%s ", j, text );
      if(c == '\n' || c == '\000' || c == '<')
        abc_end = j;
      //if(c == '%') continue; // Keep directives and inlcudes at end of tune
      if(abc_end <= abc_start)
        break;
    }

    if(flags & ABC2HTML_CLEAN_ABC)
    {
      for(j = abc_start; j < abc_end; ++j)
      {
        const char * line = lines[j];
        oyjlStringAdd( &new_abc, 0,0, "%s\n", line );
      }
    }

    oyjlStringListRelease( &lines, lines_n, free );
  }
  *abc_start_ = abc_start;
  *abc_end_   = abc_end;
  return new_abc;
}

/* search for text in a binary file; replacement for non portable memmem() */
char * abc2htmlMemstr( const char * stack, int stack_len, const char * needle, int needle_len )
{
  int i;
  char c = needle[0];
  for(i = 0; i < stack_len - needle_len; ++i)
  {
    const char * t = &stack[i];
    if(t[0] == c)
      if(strstr(t, needle) != NULL)
        return (char*)t;
  }
  return NULL;
}
char * abc2htmlMemrstr( const char * stack, int stack_len, const char * needle, int needle_len )
{
  int i;
  char c = needle[0];
  for(i = stack_len - needle_len; i >= 0; --i)
  {
    const char * t = &stack[i];
    if(t[0] == c)
      if(strstr(t, needle) != NULL)
        return (char*)t;
  }
  return NULL;
}

oyjl_val abc2htmlGetAbcRanges ( const char ** list, int n, const char * input, int flags OYJL_UNUSED )
{
  oyjl_val root = oyjlTreeNew("");
  int i,j, tuneindex = -1, X, T, K, A, start, end, len, write, last_end = 0, last_filled_line = 0, titles_n = 0, header_start = -1, header_end = -1, beginsvg = -1;
  const char * line, *titles[10] = {0,0,0,0,0, 0,0,0,0,0};
  char c;
  A = X = K = T = end = 0; start = -1;
  for(i = 0; i < n; ++i)
  {
    write = 0;
    line = list[i];
    j = 0;
    while( line[0] && isspace(line[0]) ) line++;
    c = line[0];
    len = strlen(line);
    if(len)
      last_filled_line = i;
    switch(c)
    {
      case '%': /* highest priority */
        c = line[1];
        if(c == 'a')
        {
          ++line;
          len = strlen(line);
          if(len >= 3)
          {
            if(memcmp(line, "abc", 3) == 0)
            {
              if(  strcmp( line, "abc" ) == 0 ||
                 !(len >= 7 && memcmp(line, "abcm2ps", 7) == 0) ||
                  (len >= 5 && (memcmp(line, "abc-", 4) == 0 || memcmp(line, "abc ", 4) == 0) && isdigit(line[4])) ||
                  (len >= 13 && memcmp(line, "abc-version ", 12) && isdigit(line[13]))
                )
              {
                if(start == -1)
                {
                  if(flags & ABC2HTML_VERBOSE3)
                    fprintf( stderr, "\"%s\"[%d]: %s", oyjlTermColor(oyjlBOLD,input), i, oyjlTermColorF(oyjlGREEN, "%s", list[i]) );
                } else
                {
                  fprintf( stderr, "\n" );
                  fprintf( stderr, OYJL_DBG_FORMAT "%s: \"%s\"[%d]: \"%s\" Found new tune start befor end of old tune. (empty line between tunes missing?)\n", OYJL_DBG_ARGS, oyjlTermColorF(oyjlBLUE, _("Warning")), oyjlTermColor(oyjlBOLD,input), i, list[i] );
                  if(start != i-1)
                    ++write;
                }

                if(start == -1 || start == i-1)
                  A = start = i;
              }
            }
          }
        }
        else
        {
          c = (++line)[0];
          if(c == '%')
          {
            ++line;
            while( line[0] && isspace(line[0]) ) line++;
            if(oyjlStringFind( line, "beginsvg", OYJL_COMPARE_STARTS_WITH | OYJL_COMPARE_CASE ))
              beginsvg = i;
            else
            if(oyjlStringFind( line, "endsvg", OYJL_COMPARE_STARTS_WITH | OYJL_COMPARE_CASE ))
              beginsvg = -1;
          }
        }
        break;
      case 'X': /* second priority */
        if(start == -1 && line[1] == ':' &&
            ((!isspace(line[2]) && len >= 3 && isdigit(line[2])) ||
             (isspace(line[2]) && len >= 4 && isdigit(line[3])))
          )
        {
          X = start = i;
          if(flags & ABC2HTML_VERBOSE3)
            fprintf( stderr, "\"%s\"[%d]: %s", oyjlTermColor(oyjlBOLD,input), i, oyjlTermColorF(oyjlGREEN, "%s", list[i]) );
        }
        break;
      case 'T': /* if none above */
        line += 2;
        while( line[0] && isspace(line[0]) ) line++;
        if(start == -1 && line[1] == ':' && len >= 3)
        {
          T = start = i;
          if(flags & ABC2HTML_VERBOSE3)
            fprintf( stderr, "\"%s\"[%d]: %s\n", oyjlTermColor(oyjlBOLD,input), i, oyjlTermColorF(oyjlGREEN, "%s", list[i]) );
        } else
          if(flags & ABC2HTML_VERBOSE3)
            fprintf( stderr, "\t(%s)", oyjlTermColor(oyjlITALIC,line) );

        if(titles_n <= 10)
          titles[titles_n++] = line;
        break;
      case 'K': /* informational */
        if(line[1] == ':' && len >= 3)
        {
          line += 2;
          while( line[0] && isspace(line[0]) ) line++;
          K = i;
          if(flags & ABC2HTML_VERBOSE3)
            fprintf( stderr, "\t%s", line );
        }
        break;
      case '\000':
      case '<':
        if(!end && start >= 0)
        {
          last_end = end = i-1;
          ++write;
          if(flags & ABC2HTML_VERBOSE3)
            fprintf( stderr, "\n\"%s\"[%d]: %s\n", oyjlTermColor(oyjlBOLD,input), end, list[end] );
        }

        if(c == '<' && beginsvg == -1) // we are not inside a "%% beginsvg" section
        {
          if(start == -1)
          {
            header_start = i+1;
            header_end = -1;
          }
        }
        break;
    }

    if( start == -1 && end == 0 && // outside tune
        last_filled_line == i )
    {
      header_end = i;
      if(header_start == -1)
        header_start = i;
    }

    if(write)
    {
      ++tuneindex;
      oyjlTreeSetIntF( root, OYJL_CREATE_NEW, tuneindex+1, "tunes/[%d]/X", tuneindex );
      if(header_end != -1 && header_start != -1 && header_end >= header_start)
      {
        oyjlTreeSetIntF( root, OYJL_CREATE_NEW, header_start, "tunes/[%d]/header/[0]", tuneindex );
        oyjlTreeSetIntF( root, OYJL_CREATE_NEW, header_end, "tunes/[%d]/header/[1]", tuneindex );
        header_start = header_end = -1;
      }
      oyjlTreeSetIntF( root, OYJL_CREATE_NEW, A?A:X?X:T, "tunes/[%d]/range/[0]", tuneindex );
      oyjlTreeSetIntF( root, OYJL_CREATE_NEW, end?end:i-1, "tunes/[%d]/range/[1]", tuneindex );
      for(j = 0; j < titles_n; ++j)
        oyjlTreeSetStringF( root, OYJL_CREATE_NEW, titles[j], "tunes/[%d]/tiltes/[%d]", tuneindex, j, titles[j] );
      if(end == K)
        oyjlTreeSetStringF( root, OYJL_CREATE_NEW, "header-only", "tunes/[%d]/state", tuneindex );
      A = X = T = K = end = titles_n = 0;
      start = -1;
      memset( titles, 0, sizeof(char*)*10 );
    }
  }

  if(last_end < last_filled_line && header_end >= header_start)
  {
    oyjlTreeSetIntF( root, OYJL_CREATE_NEW, header_start, "footer/[0]", tuneindex );
    oyjlTreeSetIntF( root, OYJL_CREATE_NEW, header_end, "footer/[1]", tuneindex );
  }

  if(flags & ABC2HTML_VERBOSE3 && last_end != n-2)
    fprintf( stderr, "\"%s\"[%d] %s\n", oyjlTermColor(oyjlBOLD,input), n-2, list[n-2] );

  oyjlTreeSetStringF( root, OYJL_CREATE_NEW, input, "input" );

  return root;
}

#define ABC2HTML_PDF_COMMENT_ABC "%pdf-comment-abc-code"
char * abc2htmlAbcFromPdf( const char * text, int text_len, const char * input OYJL_UNUSED, int flags OYJL_UNUSED )
{
  /* check for PDF comment */
  const char * needle = ABC2HTML_PDF_COMMENT_ABC;
  int needle_len = strlen(needle)-1, list_n,i;
  char * t, **list, * tmp_pdf = NULL;;
  if(text_len)
    t = abc2htmlMemrstr( text, text_len, needle, needle_len );
  if(t)
  {
    text = strchr(t+1,'\n') + 1;
    t = strstr(text, "startxref");
    if(t) t[0] = '\000';
    list = oyjlStringSplit2( text, "\n", NULL, &list_n, NULL, NULL );
    for(i = 0; i < list_n; ++i)
    {
      if(list[i][0])
        oyjlStringPush( &tmp_pdf, list[i]+1, 0,0 );
      oyjlStringPush( &tmp_pdf, "\n", 0,0 );
    }
    oyjlStringListRelease( &list, list_n, free );
    list_n = 0;
  }
  return tmp_pdf;
}


int abc2htmlFileFormat( const char * text, int text_len, const char * input OYJL_UNUSED, int flags OYJL_UNUSED )
{
  int format = 0;
  if(!text || text_len <= 0) return format;
  if(memcmp( text, "% abc", 5 ) == 0 || memcmp( text, "% ABC", 5 ) == 0 )
    format = ABC2HTML_ABC;
  else if(text_len > 4 && memcmp(text, "%PDF", 4) == 0)
    format = ABC2HTML_PDF;
  else if(text[0] == '<' && oyjlStringFind( text, "<html", OYJL_COMPARE_FIND_NEEDLE ) && oyjlStringFind( text, "class=\"abc-source", OYJL_COMPARE_FIND_NEEDLE ))
    format = ABC2HTML_HTML;
  else if(text[0] == '<' && oyjlStringFind( text, "<svg", OYJL_COMPARE_FIND_NEEDLE ) && oyjlStringFind( text, "\n<desc type=\"abc\">", OYJL_COMPARE_FIND_NEEDLE ))
    format = ABC2HTML_SVG;
  else if(oyjlStringFind( text, "% abc", OYJL_COMPARE_FIND_NEEDLE ) || oyjlStringFind( text, "X:", OYJL_COMPARE_FIND_NEEDLE ) || oyjlStringFind( text, "T:", OYJL_COMPARE_FIND_NEEDLE ) )
    format = ABC2HTML_ABC;
  else if(oyjlStringFind( text, "\n%", OYJL_COMPARE_FIND_NEEDLE )) // possible comment
    format = ABC2HTML_ABC_FOOTER;
  return format;
}

char * abc2htmlGetAbcLines( oyjl_val ranges, const char ** lines, int n, int index, int flags )
{
  char * text = NULL, * txt;
  oyjl_str s = oyjlStr_New(10, 0,0);
  oyjl_val tunes = oyjlTreeGetValue( ranges, 0, "tunes" ), v;
  int count = oyjlValueCount( tunes ), i,j, start, end, len;
  const char * type = "range"; /* ABC2HTML_TUNES */
  if(flags & ABC2HTML_ABC_INTRO)
    type = "header";

  for(i = 0; i < count; ++i)
  {
    if((index >= 0 && i != index)) continue;
    v = oyjlTreeGetValueF( tunes, 0, "[%d]/%s/[0]", i, type );
    start = OYJL_IS_INTEGER(v) ? OYJL_GET_INTEGER(v) : -1;
    v = oyjlTreeGetValueF( tunes, 0, "[%d]/%s/[1]", i, type );
    end = OYJL_IS_INTEGER(v) ? OYJL_GET_INTEGER(v) : -1;
    if(start >= 0 && end >= 0 && start <= end)
    {
      for(j = start; j <= end;  ++j)
        if(j < n)
        {
          oyjlStr_Push( s, lines[j] );
          oyjlStr_Push( s, "\n" );
        }
    }
  }

  if(flags & ABC2HTML_ABC_FOOTER)
  {
    type = "footer";
    v = oyjlTreeGetValueF( ranges, 0, "%s/[0]", type );
    start = OYJL_IS_INTEGER(v) ? OYJL_GET_INTEGER(v) : -1;
    v = oyjlTreeGetValueF( ranges, 0, "%s/[1]", type );
    end = OYJL_IS_INTEGER(v) ? OYJL_GET_INTEGER(v) : -1;
    if(start >= 0 && end >= 0 && start <= end)
    {
      for(j = start; j <= end;  ++j)
        if(j < n)
        {
          oyjlStr_Push( s, lines[j] );
          oyjlStr_Push( s, "\n" );
        }
    }
  }

  txt = oyjlStr_Pull( s );
  oyjlStr_Release( &s );
  len = strlen(txt);
  if(len) text = txt;
  else if(txt) free(txt);
  return text;
}

int abc2htmlAddAbc ( const char * text_, int text_len, char *** abcs, int * count, const char * input, char *** inputs, int flags, char ** abc_global_intro, char ** abc_footer )
{
  int len = text_len ? text_len : text_ ? (int)strlen(text_) : 0;
  int format = abc2htmlFileFormat( text_, len, input, flags );
  text_len = len;
  if(format)
  {
    int list_n = 0, n = *count, i, x, tunes_n = 0;
    char ** list;
    char * tmp = NULL, * tmp_svg = NULL, * tmp_pdf = NULL, * text = oyjlStringAppendN( NULL, text_, text_len?(size_t)text_len:strlen(text_), 0 ), *t = NULL, * json;
    oyjl_val root;

    tmp = text;
    if(format == ABC2HTML_PDF)
      text = tmp_pdf = abc2htmlAbcFromPdf( text_, text_len, input, flags );
    else
    {
      x = oyjlStringReplace( &text, "\r\n", "\n", 0,0 ); /* fix: DOS newline */
      tmp = text;

      if(x) fprintf( stderr, "found %d lines with \\r\\n in %s\n", x, input );
      while(text[0] == '\n') ++text; // skip leading new lines
      if(format == ABC2HTML_SVG)
      {
        /* check inside svg/desc */
        t = strstr(text, "\n<desc type=\"abc\">");
        if(t)
        {
          text = strchr(t+1,'\n');
          text = tmp_svg = mabcEscapeSvgToAbc( text );
        }
      } else if(format == ABC2HTML_HTML)
        text = strstr( text, "<div class=\"abc-source" );
    }

    list = oyjlStringSplit( text, '\n', &list_n, NULL );
    root = abc2htmlGetAbcRanges( (const char **)list, list_n, input, flags );
    if(flags & ABC2HTML_VERBOSE2)
    {
      json = oyjlTreeToText( root, OYJL_JSON );
      fprintf( stderr, OYJL_DBG_FORMAT "abc len:%d json:%d -> %s tune lines\n", OYJL_DBG_ARGS, text?(int)strlen(text):0, json?(int)strlen(json):0, json );
      if(json) {free(json); json = NULL;}
    }

    tunes_n = oyjlValueCount( oyjlTreeGetValue( root, 0, "tunes" ) );
    for(i = 0; i < tunes_n; ++i)
    {
      t = abc2htmlGetAbcLines( root, (const char **)list, list_n, i, flags | ABC2HTML_TUNES );
      oyjlStringListPush( abcs, count, t, 0,0 );
      free(t);
    }
    if(abc_global_intro)
      *abc_global_intro = abc2htmlGetAbcLines( root, (const char **)list, list_n, -1, flags | ABC2HTML_ABC_INTRO );
    if(abc_footer)
      *abc_footer = abc2htmlGetAbcLines( root, (const char **)list, list_n, -1, flags | ABC2HTML_ABC_FOOTER );

    /* Clean */
    oyjlTreeFree( root ); root = NULL;
    oyjlStringListRelease( &list, list_n, free );

    if(inputs)
      for(i = n; i < *count; ++i)
        oyjlStringListPush( inputs, &n, input, 0,0 );

    if(tmp) free(tmp);
    if(tmp_svg) free(tmp_svg);
    if(tmp_pdf) free(tmp_pdf);
  }
  return 0;
}

int oyjlLowerStrcmpInverseWrap_ (const void * a_, const void * b_)
{
  const char * a = *(const char **)b_,
             * b = *(const char **)a_;
#ifdef HAVE_POSIX
  return strcasecmp(a,b);
#else
  return strcmp(a,b);
#endif
}


#ifndef NO_MAIN
static char ** environment = NULL;
#endif
static char * xdg_user_dir_lookup (const char *type);
#include <dirent.h>
void listFiles(const char * path, char *** list, int * count, const char * suffix)

{
  DIR * d = opendir(path);
  if(d == NULL) return;
  struct dirent * dir;
  while ((dir = readdir(d)) != NULL)
    {
      if(dir->d_type != DT_DIR)
      {
        char * name = NULL; oyjlStringAdd( &name, 0,0, "%s/%s", path, dir->d_name);
        if(!suffix)
            oyjlStringListPush( list, count, name, malloc, free );
        else
        {
          char * dot = strrchr( name, '.' ),
               * end = dot ? dot+1 : NULL;
          if(end && strcasecmp(end, suffix) == 0)
            oyjlStringListPush( list, count, name, malloc, free );
        }
        free(name);
      }
      else
      if( dir->d_type == DT_DIR && strcmp(dir->d_name,".") != 0 &&
          strcmp(dir->d_name,"..") != 0 )
      {
        char * d_path = NULL; oyjlStringAdd( &d_path, 0,0, "%s/%s", path, dir->d_name);
        listFiles(d_path, list, count, suffix);
        free(d_path);
      }
    }

    closedir(d);
}

/* list from:
 * - PWD - current working dir
 * - ABC2HTML_DATADIR/abc2html - installed examples
 * - XDG ~/Music
 */
void abc2htmlListAllFiles(char *** list, int * count, const char * suffix)
{
#ifdef __ANDROID__
  const
#endif
        char * xdg =
#ifdef __ANDROID__
                  "/storage/emulated/0/Music";
#else
                  xdg_user_dir_lookup( "Music" );
#endif
  char * data_dir = NULL;
  oyjlStringAdd( &data_dir, 0,0, "%s/abc2html", ABC2HTML_DATADIR );;
        //printf("music: %s\n", xdg);
  char * pwd = getcwd(NULL,0), * t;
  int i,n;
  listFiles(pwd, list, count, suffix);
  n = *count;
  /* list lokal files in current working directory with no path or with relative path */
  if(pwd && pwd[0] && pwd[strlen(pwd)-1] != '/')
    oyjlStringPush( &pwd, "/", 0,0 );
  for(i = 0; i < n; ++i)
  {
    t = (*list)[i];
    oyjlStringReplace( &t, pwd, "", 0,0 );
    (*list)[i] = t;
  }
  free(pwd);
  if(xdg) listFiles(xdg, list, count, suffix);
  listFiles(data_dir, list, count, suffix);
  oyjlStringListFreeDoubles( *list, count, free );
  qsort( *list, (size_t)*count, sizeof(char*), oyjlLowerStrcmpInverseWrap_ );
#ifndef __ANDROID__
  if(xdg) free(xdg);
#endif
}


oyjlOptionChoice_s * getAbcChoices              ( oyjlOption_s      * o OYJL_UNUSED,
                                                  int               * selected,
                                                  oyjlOptions_s     * opts OYJL_UNUSED )
{
    int choices = 0, current = -1;
    char ** choices_string_list = NULL;
    int error = 0;

    if(!error)
    {
      int i;
      const char * man_page = getenv("DISPLAY");
      int skip_real_info = man_page && strcmp(man_page,"man_page") == 0;
      if(!skip_real_info)
        abc2htmlListAllFiles(&choices_string_list, &choices, "abc");
      oyjlOptionChoice_s * c = calloc((unsigned int)choices+1, sizeof(oyjlOptionChoice_s));
      if(c)
      {
        for(i = 0; i < choices; ++i)
        {
          char * v = malloc(12);

          sprintf(v, "%d", i);
          c[i].nick = strdup(choices_string_list[i]);
          c[i].name = strdup(choices_string_list[i]);
          c[i].description = strdup("");
          c[i].help = strdup("");
        }
        c[i].nick = malloc(4);
        c[i].nick[0] = '\000';
      }
      oyjlStringListRelease( &choices_string_list, choices, free );
      if(selected)
        *selected = current;

      return c;
    } else
      return NULL;
}

oyjlOptionChoice_s * getStyleChoices            ( oyjlOption_s      * o OYJL_UNUSED,
                                                  int               * selected,
                                                  oyjlOptions_s     * opts OYJL_UNUSED )
{
    int choices = 0, current = -1;
    char ** choices_string_list = NULL;
    int error = 0;

    if(!error)
    {
      int i;
      const char * man_page = getenv("DISPLAY");
      int skip_real_info = man_page && strcmp(man_page,"man_page") == 0;
      if(!skip_real_info)
        abc2htmlListAllFiles(&choices_string_list, &choices, "a2hstyle");
      oyjlOptionChoice_s * c = calloc((unsigned int)choices+1, sizeof(oyjlOptionChoice_s));
      if(c)
      {
        for(i = 0; i < choices; ++i)
        {
          char * v = malloc(12);

          sprintf(v, "%d", i);
          c[i].nick = strdup(choices_string_list[i]);
          c[i].name = strdup(choices_string_list[i]);
          c[i].description = strdup("");
          c[i].help = strdup("");
        }
        c[i].nick = malloc(4);
        c[i].nick[0] = '\000';
      }
      oyjlStringListRelease( &choices_string_list, choices, free );
      if(selected)
        *selected = current;

      return c;
    } else
      return NULL;
}

FILE * abc2htmlOpenOutput ( const char * output, FILE * out )
{
  if(!output || (strcmp(output,"-") == 0 || strcmp(output,"stdout") == 0))
  {
    if(!out)
      out = stdout;
    fprintf( stderr, "%s: stdout\n", _("Output"));
  }
  else
  {
    if(!out)
      out = oyjlFopen( output, "w" );
    fprintf( stderr, "%s: %s\n", _("Output"), output);
  }
  return out;
}

char * abc2html_get_path_ = NULL;
const char * abc2htmlGetPath( const char * filename )
{
  if(abc2html_get_path_) { free(abc2html_get_path_); abc2html_get_path_ = NULL; }
  if(filename) abc2html_get_path_ = oyjlStringCopy( filename, 0 );
  if(oyjlIsFile( filename, "r",0,0,0 ))
  {
    char * t = strrchr(abc2html_get_path_, '/');
    if(t) t[0] = '\000';
  }
  return abc2html_get_path_;
}

int abc2htmlGetFileSize( const char * fn )
{
  FILE * fp = oyjlFopen( fn, "r" );
  int size = 0;
  if(fp)
  {
    size = oyjlFsize(fp);
    fclose(fp);
  }
  return size;
}

char * abc2htmlReplaceFileNameEnding( const char * fn, const char * suffix )
{
  char * text = oyjlStringCopy( fn, 0 ),
       * t = strrchr( text, '.' );
  if(t) t[0] = '\000';
  t = NULL;
  oyjlStringAdd( &t, 0,0, "%s.%s", text, suffix );
  free(text);
  return t;
}

char * abc2htmlCreateNumberdFileNames( const char * fn, const char * suffix, int index )
{
  char * text = oyjlStringCopy( fn, 0 ), * t;
  t = strrchr( text, '.' );
  if(t) t[0] = '\000';
  t = NULL;
  oyjlStringAdd( &text, 0,0, "%03d.%s", index, suffix );
  return text;
}

#define ABC2SVG "abcm2ps"
char ** abc2htmlCreateSvg( const char * fn, int temp_abc_n OYJL_UNUSED, const char * output, oyjlUi_s * ui OYJL_UNUSED, int flags )
{
  char * t = NULL,
       * text = NULL,
      ** files = NULL;
  int n = 0, i;

#ifdef HAVE_MUSICABC
  int size = 0, count = 0, error, rsize = 0, pos = 0;
  char ** abcs = NULL, * abc_global_intro = NULL;
  const char * svg;

  if(!output || strcmp(output,"stdout") == 0 || strcmp(output,"-") == 0)
    output = fn;
  t = oyjlReadFile( fn, 0, &size ),
  abc2htmlAddAbc( t, 0, &abcs, &count, fn, NULL, flags, &abc_global_intro, NULL );
  if(flags & ABC2HTML_VERBOSE)
    fprintf( stderr, OYJL_DBG_FORMAT "%s count: %d\n", OYJL_DBG_ARGS, fn, count );
  mabcContext_s * mctx = mabcContext_New();
  if(flags & ABC2HTML_VERBOSE)
    mabcVerbose( 1 );
  mabcContext_SetFilename( mctx, fn );
  mabcContext_SetFormat( mctx, mabcFORMAT_SVG );
  for(i = 0; i < count; ++i)
  {
    const char * abc = abcs[i];
    text = NULL;
    if(abc_global_intro)
      oyjlStringPush( &text, abc_global_intro, 0,0 );
    oyjlStringPush( &text, abc, 0,0 );
    abc = text;
    mabcContext_SetAbcTune( mctx, abc );
    free(text); abc = NULL;
    error = mabcContext_Run( mctx, 0 );
    svg = mabcContext_GetResult( mctx, &rsize );
    if(svg)
    {
      ++pos;
      text = abc2htmlCreateNumberdFileNames( output, "svg", pos );
      oyjlWriteFile( text, svg, rsize );
      oyjlStringListPush( &files, &n, text, 0,0 );
    }
    if(flags & ABC2HTML_VERBOSE)
      fprintf( stderr, OYJL_DBG_FORMAT "[%d|err:%d]: abc:%d svg:%d -> %s\n", OYJL_DBG_ARGS, i, error, abc?(int)strlen(abc):0, svg?(int)strlen(svg):0, text?text:"----" );
    if(text) {free(text); text = NULL;}
  }
  mabcContext_Release( &mctx );
  mabcRelease();
#else
  const char * abc_intro = ""; //"--titleformat \"R-P-Q-1 T0T0 C1+O1 T1 N1\" --titlefont \"Bilbo 28\" --annotationfont \"Bilbo\" --composerfont \"Bilbo\" --historyfont \"Bilbo\" --infofont \"Bilbo\" --measurefont \"Bilbo\" --partsfont \"Bilbo\" --repeatfont \"Bilbo\" --subtitlefont \"Bilbo\" --tempofont \"Bilbo 18\" --textfont \"Bilbo\" --voicefont \"Bilbo\" --wordsfont \"mono 10\" --infoname \"B Liederbuch: \" --infoname \"S Quelle: \" --infoname \"D Discography: \" --infoname \"N Noten: \" --infoname \"Z Transkript: \" --infoname \"H Geschichte: \" --writefields \"COPQTABSDNZHWw\" -s \"1.1\" --staffwidth \"24cm\"";
  if(oyjlHasApplication(ABC2SVG))
  {
    int size = 0;
    if(!output || strcmp(output,"stdout") == 0 || strcmp(output,"-") == 0)
      output = fn;
    t = oyjlReadCommandF( &size, flags&ABC2HTML_VERBOSE?"r.verbose":"r", malloc, ABC2SVG " -g %s %s %s -O %s -j 0 %s", abc_intro, 
        "", /*flags & ABC2HTML_FIRST_LYRIC_ONLY ? "--gchordfont \"serif 12\" --capofont \"serif 36\" --vocalspace \"0\" --vocalfont \"sans-serif 16\" --staffscale \"0.3\" --staffsep \"40\""
                    : "--gchordfont \"Bilbo 24\" --vocalfont \"Bilbo 16\"",*/
        flags & ABC2HTML_BORDERLESS ? "--topmargin 0.1cm --botmargin 0.1cm --leftmargin 0.1cm --rightmargin 0.1cm" : "",
        output, fn );
    if(t) { free(t); t = NULL; }
    for(i = 0; i < temp_abc_n; ++i)
    {
      text = abc2htmlCreateNumberdFileNames( output, "svg", i+1 );
      oyjlStringListPush( &files, &n, text, 0,0 );
      if(flags & ABC2HTML_VERBOSE)
        fprintf(stderr, OYJL_DBG_FORMAT "%s %s\n", OYJL_DBG_ARGS, _("Generated"), oyjlTermColorF( oyjlBOLD, "%s", text ) );
      if(text) { free(text); text = NULL; }
    }
  }
  else
  {
    int count = 0, i;
    char ** results = oyjlOptions_ResultsToList( ui->opts, NULL, &count );
    int pos = oyjlStringListFind( results, &count, "-f=svg", OYJL_COMPARE_LAZY, 0 );
    fprintf( stderr, "%s", ui->opts->argv[0] );
    for(i = 0; i < count; ++i) fprintf( stderr, " %s", oyjlTermColor( pos == i?oyjlRED:oyjlNO_MARK, results[i] ) );
    fprintf( stderr, "\n" );
    oyjlStringListRelease( &results, count, free );
    fprintf(stderr, OYJL_DBG_FORMAT "%s:\t%s %s\n", OYJL_DBG_ARGS, oyjlTermColorF(oyjlRED, "%s", _("Error")), oyjlTermColorFPtr(oyjlRED, &t, ABC2SVG ": %s", _("not available")), _("The program is required for rendering.") );
  }
#endif

  if(t) free(t);
  return files;
}

#ifdef _OPENMP
#define USE_OPENMP 1
#include <omp.h>
#else
#warning "no openmp optimisation"
#endif

#define ABC2PDF "inkscape"
#define ABC2PDF_ARGS "--export-pdf "
#define ABC2PDFB "rsvg-convert"
#define ABC2PDFB_ARGS "-f=pdf -o "
#define ABC2PDF2 "pdftk"
char * abc2htmlCreatePdf( const char ** svgs, int svgs_n, const char * output, oyjlUi_s * ui, int flags )
{
  char * t = NULL, ** pdfs = NULL, * pdfs_arg = NULL, * fn = NULL;
  int i, pdfs_n = 0, size = 0, index = -1,
      verbose = flags & ABC2HTML_VERBOSE;
  const char * convert = ABC2PDFB, * convert_args = ABC2PDFB_ARGS;

  /* take inkscape as fallback */
  if(!oyjlHasApplication(convert)) { convert = ABC2PDF; convert_args = ABC2PDF_ARGS; }
  if(!oyjlHasApplication(convert) || !oyjlHasApplication(ABC2PDF2))
  {
    int count = 0, i;
    char ** results = oyjlOptions_ResultsToList( ui->opts, NULL, &count );
    int pos = oyjlStringListFind( results, &count, "-f=pdf", OYJL_COMPARE_LAZY, 0 );
    fprintf( stderr, "%s", ui->opts->argv[0] );
    for(i = 0; i < count; ++i) fprintf( stderr, " %s", oyjlTermColor( pos == i?oyjlRED:oyjlNO_MARK, results[i] ) );
    fprintf( stderr, "\n" );
    oyjlStringListRelease( &results, count, free );
    if(!oyjlHasApplication(ABC2PDFB))
      fprintf(stderr, OYJL_DBG_FORMAT "%s:\t%s %s\n", OYJL_DBG_ARGS, oyjlTermColorF(oyjlRED, "%s", _("Error")), oyjlTermColorFPtr(oyjlRED, &t, ABC2PDFB ": %s", _("not available")), _("The program is required for rendering.") );
    if(!oyjlHasApplication(ABC2PDF2))
      fprintf(stderr, OYJL_DBG_FORMAT "%s:\t%s %s\n", OYJL_DBG_ARGS, oyjlTermColorF(oyjlRED, "%s", _("Error")), oyjlTermColorFPtr(oyjlRED, &t, ABC2PDF2 ": %s", _("not available")), _("The program is required for rendering.") );
    return NULL;
  }

  for( i = 0; i < svgs_n; ++i )
  {
    const char * svg = svgs[i];

    fn = abc2htmlReplaceFileNameEnding( svg, "pdf" );

    oyjlStringListPush( &pdfs, &pdfs_n, fn, 0,0 );
    oyjlStringAdd( &pdfs_arg, 0,0, " %s", fn );

    if(fn) { free(fn); fn = NULL; }
  }

#if defined(USE_OPENMP)
#pragma omp parallel for private(index, fn, t)
#endif
  for( i = 0; i < svgs_n; ++i )
  {
    const char * svg = svgs[i];
#if defined(_OPENMP) && defined(USE_OPENMP)
    index = omp_get_thread_num();
#endif

    fn = pdfs[i];

    /* Start one after the other. inkscape gives sometimes errors if started simultaniously */
    t = oyjlReadCommandF( &size, verbose?"r.verbose":"r", malloc, "sleep 0.%d", index );
    if(t) { free(t); t = NULL; }
    t = oyjlReadCommandF( &size, verbose?"r.verbose":"r", malloc, "%s %s %s %s",
                          convert, svg, convert_args, fn );
    if(verbose)
      fprintf(stderr, "[%d] %s Intermediate: %s index: %d\n", i+1, _("Generated"), oyjlTermColor( oyjlBOLD, fn ), index );
    if(!oyjlIsFile( fn, "r",0,0,0 ))
      fprintf(stderr, "[%d] %s %s Intermediate: %s failed\n", i+1, oyjlTermColor( oyjlRED, _("Error:")), _("Generated"), fn );

    if(t) { free(t); t = NULL; }
  }

  t = oyjlReadCommandF( &size, verbose?"r.verbose":"r", malloc, ABC2PDF2 " %s output %s", pdfs_arg, output );

  if(!verbose && !(flags & ABC2PDF_KEEP_INTERMEDIATE))
    for( i = 0; i < pdfs_n; ++i )
      /* erase temporary pdf file */
      oyjlWriteFile( pdfs[i], NULL, 0 );
  if(t) { free(t); t = NULL; }

  fn = abc2htmlReplaceFileNameEnding( output, "txt" );
  t = oyjlReadCommandF( &size, verbose?"r.verbose":"r", malloc, ABC2PDF2 " %s dump_data output %s", output, fn );
  if(t)
  {
    if(verbose)
      fprintf(stderr, "%s Intermediate: %s:\n", _("Generated"), oyjlTermColor( oyjlBOLD, fn ) );
  }
  
  oyjlStringListRelease( &pdfs, pdfs_n, 0 );

  if(t) { free(t); t = NULL; }
  if(pdfs_arg) { free(pdfs_arg); pdfs_arg = NULL; }

  return fn; /* meta data */
}

#define ABC2PNG_ARGS "--export-png "
#define ABC2PNGB_ARGS "-f=png --background-color=white --keep-aspect-ratio -o "
char * abc2htmlCreatePng( const char * svg, int pixel, oyjlUi_s * ui, int flags )
{
  char * t = NULL, * fn = NULL;
  int size = 0,
      verbose = flags & ABC2HTML_VERBOSE;
  const char * convert = ABC2PDFB, * convert_args = ABC2PNGB_ARGS;

  /* take inkscape as fallback */
  if(!oyjlHasApplication(convert)) { convert = ABC2PDF; convert_args = ABC2PNG_ARGS; }
  if(!oyjlHasApplication(convert))
  {
    int count = 0, i;
    char ** results = oyjlOptions_ResultsToList( ui->opts, NULL, &count );
    int pos = oyjlStringListFind( results, &count, "-f=png", OYJL_COMPARE_LAZY, 0 );
    fprintf( stderr, "%s", ui->opts->argv[0] );
    for(i = 0; i < count; ++i) fprintf( stderr, " %s", oyjlTermColor( pos == i?oyjlRED:oyjlNO_MARK, results[i] ) );
    fprintf( stderr, "\n" );
    oyjlStringListRelease( &results, count, free );
    if(!oyjlHasApplication(ABC2PDFB))
      fprintf(stderr, OYJL_DBG_FORMAT "%s:\t%s %s\n", OYJL_DBG_ARGS, oyjlTermColorF(oyjlRED, "%s", _("Error")), oyjlTermColorFPtr(oyjlRED, &t, ABC2PDFB ": %s", _("not available")), _("The program is required for rendering.") );
    return NULL;
  }

  fn = abc2htmlReplaceFileNameEnding( svg, "png" );

  /* Start one after the other. inkscape gives sometimes errors if started simultaniously */
  t = oyjlReadCommandF( &size, verbose?"r.verbose":"r", malloc, "%s %s --width=%d %s %s",
                        convert, svg, pixel, convert_args, fn );
  if(verbose)
    fprintf(stderr, "[%d] %s Intermediate: %s\n", 1, _("Generated"), oyjlTermColor( oyjlBOLD, fn ) );
  if(!oyjlIsFile( fn, "r",0,0,0 ))
    fprintf(stderr, "[%d] %s %s Intermediate: %s failed\n", 1, oyjlTermColor( oyjlRED, _("Error:")), _("Generated"), fn );

  if(t) { free(t); t = NULL; }
  return fn; /* meta data */
}

const char * abc2htmlReadFile( const char * abc_fn, const char * style_abc, const char * style, const char * static_abc, char ** abc_in_tmp, const char *variable_name, int fflags, int verbose )
{
  if(abc_fn)
  {
    int size = 0;
    if(verbose)
      fprintf( stderr, "%s \"%s\" ", _("Read"), oyjlTermColor(oyjlITALIC,abc_fn) );
    if(oyjlIsFile( abc_fn, "r", 0,0,0 ))
      abc_fn = *abc_in_tmp = oyjlReadFile( abc_fn,  OYJL_IO_RESOLVE | OYJL_IO_STREAM, &size );
    else
    {
      *abc_in_tmp = oyjlStringCopy( abc_fn, 0 );
      oyjlStringReplace( abc_in_tmp, "\\n", "\n", 0,0 );
      abc_fn = *abc_in_tmp;
      size = strlen(abc_fn);
    }
    if(verbose && size)
      fprintf( stderr, " %d byte\n", size );
  }
  else if(style_abc)
  {
    abc_fn = style_abc;
    if(verbose)
      fprintf( stderr, "%s %s <- \"%s\"\n", _("Use"), variable_name, oyjlTermColor(oyjlITALIC,style) );
  }
  else if(fflags & ABC2HTML_HTML)
  {
    abc_fn = static_abc;
    if(verbose)
      fprintf( stderr, "%s static %s\n", _("Use"), variable_name );
  }
  return abc_fn;
}

void abc2htmlEmbedAbcCodeIntoPDF( const char * fn, const char * abc, int flags OYJL_UNUSED )
{
  /* embed ABC code */
  const char * pdf_x_ref_term = "\nstartxref\n";
  int size = 0,
      abc_len = abc?strlen(abc):0,
      pos = 0, n = 0,i,len = 0;
  char * data = abc_len?oyjlReadFile( fn, 0, &size ):NULL,
       * pdf_end, * t = NULL, * new_data, ** list;
  if(size)
    t = abc2htmlMemrstr( data, size, pdf_x_ref_term, strlen(pdf_x_ref_term) );
  if(t && abc_len)
  {
    pos = t - data;
    pdf_end = oyjlStringCopy( t, 0 ); // remember PDF xref
    new_data = calloc( sizeof(char), size + 2 * abc_len );
    memcpy( new_data, data, size ); // write most of the PDF
    list = oyjlStringSplit( abc, '\n', &n, 0 );
    len = strlen(ABC2HTML_PDF_COMMENT_ABC);
    t = &new_data[pos];
    memcpy( &new_data[pos++], "\n", 1 );
    memcpy( &new_data[pos], ABC2HTML_PDF_COMMENT_ABC, len );
    pos += len;
    memcpy( &new_data[pos++], "\n", 1 );
    for(i = 0; i < n; ++i)
    {
      len = strlen(list[i]);
      if(!i && !len) continue;
      memcpy( &new_data[pos++], "%", 1 ); // convert to a PDF comment
      memcpy( &new_data[pos], list[i], len ); // add ABC line
      pos += len;
      memcpy( &new_data[pos++], "\n", 1 );
    }
    len = strlen(pdf_end);
    memcpy( &new_data[pos], pdf_end+1, len-1 ); // take care of the first newline
    pos += len - 1;
    oyjlWriteFile( fn, new_data, pos );
    free(new_data);
    oyjlStringListRelease( &list, n, free );
    free(pdf_end);
  }
}

int abc2htmlRenameFileToTitle( const char * fn, const char ** files, const char * src_end, const char * dest_end, int fflags, int flags )
{
  int size = 0, n = 0, i, count = 0;
  char ** abcs = NULL;

  char * t = oyjlReadFile( fn, 0, &size );
  abc2htmlAddAbc( t, 0, &abcs, &count, fn, NULL, flags, NULL,NULL );
  if(flags & ABC2HTML_VERBOSE)
    fprintf( stderr, OYJL_DBG_FORMAT "%s count: %d\n", OYJL_DBG_ARGS, fn, count );

  for(i = 0; i < count; i++)
  {
    char * tmp_output = NULL, * T;
    const char * abc = abcs[i];
    T = abc2htmlGetAbcField( abc, "T:", 0 );
    if(T)
    {
      char * fn = NULL;
      const char * input = files[i];
      if(src_end)
        input = fn = abc2htmlReplaceFileNameEnding( input, src_end );
      oyjlStringAdd( &tmp_output, 0,0, "%s.%s", T+2+(T[2]==' '?1:0), dest_end );
      rename(input, tmp_output);
      if(strcmp(dest_end,"pdf") == 0)
        abc2htmlEmbedAbcCodeIntoPDF( tmp_output, abc, flags );
      if(fn) { free(fn); fn = NULL; }
      ++n;
    }
    fprintf(stderr, "%s %s (%d %s)\n", _("Generated"), tmp_output, abc2htmlGetFileSize(tmp_output), _("bytes") );
    if(T) free(T);
    if(tmp_output) free(tmp_output);
    tmp_output = NULL;
    if(!(fflags & ABC2HTML_MULTIPAGE))
      break;
  }
  if(n != count)
    fprintf(stderr, "%s %d not all %d\n", _("Generated"), n, count );
  oyjlStringListRelease( &abcs, count, free );
  if(t) free(t);
  return count;
}

/* This function is called the
 * * first time for GUI generation and then
 * * for executing the tool.
 */
int myMain( int argc, const char ** argv )
{
  const char * input = NULL;
  char * input_tmp = NULL;
  const char * output = NULL;
  char * output_tmp = NULL;
  FILE * out = NULL;
  const char * out_name = NULL;
  const char * title = NULL;
  const char * abc_intro = NULL;
  const char * ifnot_grid2 = NULL;
  const char * if_grid2 = NULL;
  const char * abc_priority = NULL;
  const char * css_layout2 = NULL;
  const char * width = "26cm";
  const char * style = NULL;
  int high_res = 0;
  const char * list = NULL;
  int list_files = 0;
  int list_paths = 0;
  const char * format = NULL;
  const char * filter = NULL;
  double select = -1.0;
  int borderless = 0;
  const char * remove = NULL;
  char * text = NULL;
  char ** abcs = NULL;
  char ** inputs = NULL;
  int * indexes = NULL;
  int count = 0;

  int error = 0;
  int state = 0;
  const char * help = NULL;
  int verbose = 0;
  int version = 0;
  const char * render = NULL;
  const char * export_var = 0;

  /* handle options */
  /* Select a nick from *version*, *manufacturer*, *copyright*, *license*,
   * *url*, *support*, *download*, *sources*, *oyjl_module_author* and
   * *documentation*. Choose what you see fit. Add new ones as needed. */
  oyjlUiHeaderSection_s sections[] = {
    /* type, nick,            label, name,                     description  */
    {"oihs", "version",       NULL,  ABC2HTML_VERSION_NAME,    ABC2HTML_GIT_VERSION LIBMUSICABC},
    {"oihs", "manufacturer",  NULL,  _("Kai-Uwe Behrmann"),    "ku.b@gmx.de"},
    {"oihs", "copyright",     NULL,  "Copyright © 2016-2025 Kai-Uwe Behrmann", NULL },
    {"oihs", "license",       NULL,  "AGPL-3.0", "https://opensource.org/licenses/AGPL-3.0" },
    {"oihs", "oyjl_module_author", NULL, "Kai-Uwe Behrmann",   "ku.b@gmx.de"},
    {"oihs", "documentation", NULL,  NULL,                     _("The tool packs abc music notation file(s) into a html page for notes reading and listening. Informations are provided for listing titles, files and paths. Filtering of ABC files is possible.")},
    {"oihs", "support",       NULL,  NULL,                     "https://www.gitlab.com/beku/abc2html/issues"},
    {"oihs", "date",          NULL,  ABC2HTML_GIT_DATE,                     _(ABC2HTML_GIT_DATE_LONG)},
    /* use newline separated permissions in name + write newline separated list in description; both lists need to match in count */
    {"oihs", "permissions",   NULL,  "android.permission.READ_EXTERNAL_STORAGE\nandroid.permission.WRITE_EXTERNAL_STORAGE", _("Read external storage for global data access, like downloads, music ...\nWrite external storage to create and modify global data.")},
    {"",0,0,0,0}};

  /* declare the option choices  *   nick,          name,               description,                  help */
  oyjlOptionChoice_s f_choices[] = {{"ABC",         "ABC",              _("Strip HTML processing lines"),  _("The \"-r\" option allows to fine tune by extra filters.")},
                                    {"HTML",        "HTML",             _("Generate a self contained HTML Page"), _("See as well the -t, -s, -a and -c Options")},
#ifdef HAVE_MUSICABC
                                    {"SVG",         "SVG",              _("Generate a SVG Document"), _("Use libMusicAbc")},
                                    {"PNG",         "PNG",              _("Generate a PNG Document"), _("Requires rsvg-convert")},
                                    {"PDF",         "PDF",              _("Generate a PDF Document"), _("Requires rsvg-convert and pdftk")},
#else
                                    {"SVG",         "SVG",              _("Generate a SVG Document"), _("Requires abcm2ps")},
                                    {"PNG",         "PNG",              _("Generate a PNG Document"), _("Requires abcm2ps and rsvg-convert")},
                                    {"PDF",         "PDF",              _("Generate a PDF Document"), _("Requires abcm2ps, rsvg-convert and pdftk")},
#endif
                                    {NULL,NULL,NULL,NULL}};
  oyjlOptionChoice_s r_choices[] = {{"extra_abc",   "extra_abc",        _("Remove %% inside"),        _("Clean inside ABC. Useful for plain reading.")},
                                    {"-extra_abc",  "-extra_abc",       _("Skip %%"),                 _("Keep additional ABC code at start and end of ABC. Shows ABC injections from HTML page.")},
                                    {"-html",       "-html",            _("Skip HTML"),               _("Keep HTML in place.")},
                                    {NULL,NULL,NULL,NULL}};
  oyjlOptionChoice_s F_choices[] = {{"header=C=trad","header=C=trad",   _("Select C"),                _("Select only traditional composers")},
                                    {"header=R=Jig,C=Trad","header=R=Jig,C=Trad",_("Select R+C"),     _("Select only tradional jigs.")},
                                    {"select=[ 1, 2, 3, 5 ]","select=[ 1, 2, 3, 5 ]",_("Select Tunes"),_("Select by Position or Title.")},
                                    {NULL,NULL,NULL,NULL}};
  oyjlOptionChoice_s A_choices[] = {{_("List Music ABC Titles"),_("abc2html -l -i=my.abc"),NULL, NULL},
                                    {_("List Music ABC Titles and show T and R header fields"),_("abc2html -l=header=T,R -i=my.abc"),NULL, NULL},
                                    {_("List Music ABC Files"),_("abc2html --list-files -i=~/Downloads"), NULL, NULL},
                                    {_("List Music ABC Search Paths"),_("abc2html -P -i=~"), NULL,       NULL},
                                    {_("Convert Music ABC embedded in HTML to ABC"),_("abc2html -f=abc -i=my.abc.html"),NULL, NULL},
                                    {_("Convert Music ABC to HTML"),_("abc2html -f=html -i=my.abc -o=my.abc.html"),NULL, NULL},
                                    {_("Convert Music ABC to SVG"),_("abc2html -f=svg -i=my.abc -F=select=[11] -o=my.abc.svg"),NULL, NULL},
                                    {_("Convert Music ABC to PDF"),_("abc2html -f=pdf -i=my.abc -F=select=[11,\"Title\",3] -o=my.abc.pdf"),NULL, NULL},
                                    {NULL,NULL,NULL,NULL}};
  oyjlOptionChoice_s E_choices[] = {{_("F: file-name.[png|svg|jpg]"),_("The three image formats are supported for the --format=HTML output."), _("The F headline is repeatable."), NULL},
                                    {NULL,NULL,NULL,NULL}};
  oyjlOptionChoice_s S_choices[] = {{_("ABC Music Format"),"https://abcnotation.com", NULL,              NULL},
                                    {"abcm2ps(1)",  "http://moinejf.free.fr/", NULL,                     NULL},
                                    {"ABC_UI", "http://dev.music.free.fr/index.html", NULL,              NULL},
                                    {"Musyng Kite Soundfont", "https://gleitz.github.io/midi-js-soundfonts/",NULL,NULL},
                                    {NULL,NULL,NULL,NULL}};

  /* declare options - the core information; use previously declared choices */
  oyjlOption_s oarray[] = {
  /* type,   flags,                      o,  option,          key,      name,          description,                  help, value_name,         
        value_type,              values,             variable_type, variable_name, properties */
    {"oiwi", OYJL_OPTION_FLAG_EDITABLE|OYJL_OPTION_FLAG_IMMEDIATE,  "i","input",         NULL,     _("Set Input"),_("File or Stream"),          _("A file name or a input stream like \"stdin\"."), _("FILENAME"),
        oyjlOPTIONTYPE_FUNCTION, {.getChoices = getAbcChoices}, oyjlSTRING, {.s = &input}, NULL },
    {"oiwi", OYJL_OPTION_FLAG_EDITABLE,  "o","output",        NULL,     _("Output"),   _("File or Stream"),          _("A file name or a output stream like \"stdout\". The equal sign '=' (-o==) will with HTML be used for reusing the input file name with the appropriate ending. All other types will be renamed to the T: ABC title with '=' (-o==)."),_("FILENAME"),      
        oyjlOPTIONTYPE_FUNCTION, {0},                oyjlSTRING,    {.s=&output}, NULL},
    {"oiwi", OYJL_OPTION_FLAG_IMMEDIATE, "f","format",        NULL,     _("Use Format"),_("Write to specified data format."), NULL, _("FORMAT"),
        oyjlOPTIONTYPE_CHOICE,   {.choices.list = (oyjlOptionChoice_s*) oyjlStringAppendN( NULL, (const char*)f_choices, sizeof(f_choices), malloc )}, oyjlSTRING,    {.s=&format}, NULL},
    {"oiwi", OYJL_OPTION_FLAG_ACCEPT_NO_ARG|OYJL_OPTION_FLAG_EDITABLE|OYJL_OPTION_FLAG_IMMEDIATE,  "r","remove", NULL, _("Remove"), _("Remove Instructions"), _("Suboption header with comma ',' separated list of filters."), _("INSTRUCTION"),
        oyjlOPTIONTYPE_CHOICE,   {.choices.list = (oyjlOptionChoice_s*) oyjlStringAppendN( NULL, (const char*)r_choices, sizeof(r_choices), malloc )}, oyjlSTRING, {.s=&remove}, NULL },
    {"oiwi", OYJL_OPTION_FLAG_EDITABLE|OYJL_OPTION_FLAG_IMMEDIATE,  "F","filter",NULL,_("Filter"),_("Select by Filter \"header\" or \"select\""),_("Suboption header with comma ',' separated list of fields with possibly '=' assigned filter."), _("INSTRUCTION"),
        oyjlOPTIONTYPE_CHOICE,   {.choices.list = (oyjlOptionChoice_s*) oyjlStringAppendN( NULL, (const char*)F_choices, sizeof(F_choices), malloc )}, oyjlSTRING, {.s=&filter}, NULL },
    {"oiwi", OYJL_OPTION_FLAG_EDITABLE|OYJL_OPTION_FLAG_IMMEDIATE,  NULL,"select",NULL,_("Select"),_("Select Single Tune"), NULL, _("NUMBER"),
        oyjlOPTIONTYPE_DOUBLE,   {.dbl.start = 0.0, .dbl.end = 60.0, .dbl.tick = 1.0, .dbl.d = 0}, oyjlDOUBLE, {.d=&select}, NULL },
    {"oiwi", OYJL_OPTION_FLAG_IMMEDIATE, "b","borderless",    NULL,     _("Borderless"),_("Skip page borders."),     _("Applies to formats -f=svg and -f=pdf only."), NULL,
        oyjlOPTIONTYPE_NONE,     {0},                oyjlINT,       {.i=&borderless}, NULL},
    {"oiwi", OYJL_OPTION_FLAG_ACCEPT_NO_ARG|OYJL_OPTION_FLAG_EDITABLE,"l","list",NULL,_("List"),_("List"),           _("Accepts display hints of header lines. Example: -l=header=T,R,C . Add ,1 for one tune per line display. E.g -l=header=K,T,1 ."), _("HINTS"),
        oyjlOPTIONTYPE_CHOICE,   {0},                oyjlSTRING,    {.s=&list},    NULL },
    {"oiwi", OYJL_OPTION_FLAG_EDITABLE|OYJL_OPTION_FLAG_IMMEDIATE,  "t","title",         NULL,     _("Set Title"),_("Page Title"),               _("By default it will be taken from output or input."), _("NAME"),
        oyjlOPTIONTYPE_FUNCTION, {0},                oyjlSTRING,    {.s = &title}, NULL },
    {"oiwi", OYJL_OPTION_FLAG_EDITABLE|OYJL_OPTION_FLAG_IMMEDIATE,  "a","abc-intro",NULL,     _("Set ABC Header"),_("Use MusicABC Header File"),    _("Select a file to use as abc_intro."), _("FILENAME"),
        oyjlOPTIONTYPE_FUNCTION, {0},                oyjlSTRING,    {.s = &abc_intro}, NULL },
    {"oiwi", OYJL_OPTION_FLAG_EDITABLE|OYJL_OPTION_FLAG_IMMEDIATE,  NULL,"ifnot-grid2",NULL,     _("Set ABC if not grid2"),_("Use MusicABC Header File"),    _("Select a file to use as ifnot_grid2."), _("FILENAME"),
        oyjlOPTIONTYPE_FUNCTION, {0},                oyjlSTRING,    {.s = &ifnot_grid2}, NULL },
    {"oiwi", OYJL_OPTION_FLAG_EDITABLE|OYJL_OPTION_FLAG_IMMEDIATE,  NULL,"if-grid2",NULL,     _("Set ABC if grid2"),_("Use MusicABC Header File"),    _("Select a file to use as if_grid2."), _("FILENAME"),
        oyjlOPTIONTYPE_FUNCTION, {0},                oyjlSTRING,    {.s = &if_grid2}, NULL },
    {"oiwi", OYJL_OPTION_FLAG_EDITABLE|OYJL_OPTION_FLAG_IMMEDIATE,  NULL,"abc-priority",NULL,     _("Set ABC Priority"),_("Use MusicABC Header File"),    _("Select a file to use with slight priority."), _("FILENAME"),
        oyjlOPTIONTYPE_FUNCTION, {0},                oyjlSTRING,    {.s = &abc_priority}, NULL },
    {"oiwi", OYJL_OPTION_FLAG_EDITABLE,  "c","css-layout2",   NULL,     _("Set CSS"),_("Use HTML CSS File"),         _("Select a file to use as css_layout2."), _("FILENAME"),
        oyjlOPTIONTYPE_FUNCTION, {0},                oyjlSTRING,    {.s = &css_layout2}, NULL },
    {"oiwi", OYJL_OPTION_FLAG_EDITABLE|OYJL_OPTION_FLAG_IMMEDIATE,  "s","style",         NULL,     _("Set Style"),_("Use HTML CSS + ABCheader File"),_("Select a file to use as css_layout2 + abc_intro."), _("FILENAME"),
        oyjlOPTIONTYPE_FUNCTION, {.getChoices = getStyleChoices},oyjlSTRING,    {.s = &style}, NULL },
    {"oiwi", 0,                          NULL,"high-res",     NULL,     _("High Resolution"),NULL,                   NULL, NULL,
        oyjlOPTIONTYPE_NONE,     {0},                oyjlINT,       {.i = &high_res}, NULL },
    {"oiwi", 0,                          NULL,"list-files",   NULL,     _("List Files"), _("List Music ABC Files"), NULL, NULL, oyjlOPTIONTYPE_NONE, {0}, oyjlINT, {.i=&list_files}, NULL },
    {"oiwi", 0,                          "P","list-paths",    NULL,     _("List Paths"), _("List Music ABC Search Paths"), NULL, NULL, oyjlOPTIONTYPE_NONE, {0}, oyjlINT, {.i=&list_paths}, NULL },
    /* default options -h and -v */
    {"oiwi", OYJL_OPTION_FLAG_ACCEPT_NO_ARG, "h", "help", NULL, NULL, NULL, NULL, NULL, oyjlOPTIONTYPE_CHOICE, {0}, oyjlSTRING, {.s=&help}, NULL },
    {"oiwi", 0, "v", "verbose", NULL, _("Verbose"), _("Verbose"), NULL, NULL, oyjlOPTIONTYPE_NONE, {0}, oyjlINT, {.i=&verbose}, NULL },
    /* The --render option can be hidden and used only internally. */
    {"oiwi", OYJL_OPTION_FLAG_EDITABLE|OYJL_OPTION_FLAG_MAINTENANCE, "R", "render",  NULL, _("render"),  _("Render"),  NULL, NULL, oyjlOPTIONTYPE_CHOICE, {0}, oyjlSTRING, {.s=&render}, NULL },
    {"oiwi", 0, "V", "version", NULL, _("Version"), _("Version"), NULL, NULL, oyjlOPTIONTYPE_NONE, {0}, oyjlINT, {.i=&version}, NULL },
    /* default option template -X|--export */
    {"oiwi", 0, "X", "export", NULL, NULL, NULL, NULL, NULL, oyjlOPTIONTYPE_CHOICE, {.choices = {NULL, 0}}, oyjlSTRING, {.s=&export_var}, NULL },
    {"oiwi", 0,    NULL, "synopsis",NULL, NULL,         NULL,                NULL, NULL,          oyjlOPTIONTYPE_NONE, {0},oyjlNONE, {0}, NULL },
    {"oiwi", 0,                          "A","man-examples",  NULL,     _("Examples"), NULL,                         NULL, NULL,               
        oyjlOPTIONTYPE_CHOICE,   {.choices = {(oyjlOptionChoice_s*) oyjlStringAppendN( NULL, (const char*)A_choices, sizeof(A_choices), malloc ), 0}}, oyjlNONE,{}, NULL},
    {"oiwi", 0,                          "E","man-syntax",  NULL,     _("Extra Music ABC Syntax"), NULL,                         _("These ABC Music Syntax variants are specific to abc2html and not widely supported."), NULL,               
        oyjlOPTIONTYPE_CHOICE,   {.choices = {(oyjlOptionChoice_s*) oyjlStringAppendN( NULL, (const char*)E_choices, sizeof(E_choices), malloc ), 0}}, oyjlNONE,{}, NULL},
    {"oiwi", 0,                          "S","man-see_also",  NULL,     _("See Also"),NULL,                      NULL, NULL,
        oyjlOPTIONTYPE_CHOICE,   {.choices.list = (oyjlOptionChoice_s*) oyjlStringAppendN( NULL, (const char*)S_choices, sizeof(S_choices), malloc )}, oyjlNONE,{}, NULL},
    {"",0,0,NULL,NULL,NULL,NULL,NULL, NULL, oyjlOPTIONTYPE_END, {0},oyjlNONE,{0},0}
  };

  /* declare option groups, for better syntax checking and UI groups */
  oyjlOptionGroup_s groups[] = {
  /* type,   flags, name,               description,                  help,               mandatory,     optional,      detail,        properties */
    {"oiwg", 0,     NULL,               _("Conversion"),              _("Convert Music ABC to various formats. Extract ABC back, when embedded as text form in HTML. ABC selection is done by lists."), "f", "i,F,select,r,o,t,s,a,ifnot-grid2,if-grid2,abc-priority,c,b,v", "f,F,select,r,i,o,t,s,a,ifnot-grid2,if-grid2,abc-priority,c,b", 0},
    {"oiwg", 0,     NULL,               _("Information"),             _("Create Lists of ABC titles and show file locations and search paths."),                  "l|P|list-files","i,F,v",     "l,i,P,list-files", 0},
    {"oiwg", OYJL_GROUP_FLAG_GENERAL_OPTS, _("Misc"), _("General options"), NULL,         "X|h|V|R",     "v",           "X,h,V,R,v",   0},
    {"",0,0,0,0,0,0,0,0}
  };

  oyjlUi_s * ui = oyjlUi_Create( argc, argv, /* argc+argv are required for parsing the command line options */
                                       "abc2html", _("ABC Conversion"), _("Light weight ABC collection parser and conversion tool."),
#ifdef __ANDROID__
                                       ":/images/logo.svg", // use qrc
#else
                                       "abc",
#endif
                                       sections, oarray, groups, &state );
  if(ui) ui->app_type = "lib";
  if( state & oyjlUI_STATE_EXPORT && !ui )
    goto clean_main;
  if(state & oyjlUI_STATE_HELP)
  {
    fprintf( stderr, "%s\n\tman abc2html\n\n", _("For more information read the man page:") );
    goto clean_main;
  }

  if(ui && verbose)
  {
    char * json = oyjlOptions_ResultsToJson( ui->opts, OYJL_JSON );
    if(json)
    {
      fputs( json, stderr );
      free(json);
    }
    fputs( "\n", stderr );

    int i;
    char ** results = oyjlOptions_ResultsToList( ui->opts, NULL, &count );
    for(i = 0; i < count; ++i) fprintf( stderr, "%s\n", results[i] );
    oyjlStringListRelease( &results, count, free );
    fputs( "\n", stderr );
  }

  if(ui && (export_var && strcmp(export_var,"json+command") == 0))
  {
    char * json = oyjlUi_ToText( ui, oyjlARGS_EXPORT_JSON, 0 ),
         * json_commands = NULL;
    oyjlStringAdd( &json_commands, malloc, free, "{\n  \"command_set\": \"%s\",", argv[0] );
    oyjlStringAdd( &json_commands, malloc, free, "%s", &json[1] ); /* skip opening '{' */
    puts( json_commands );
    goto clean_main;
  }

  /* Render boilerplate */
  if(ui && render)
  {
#if !defined(NO_OYJL_ARGS_RENDER)
    int debug = verbose;
    oyjlTermColorInit( OYJL_RESET_COLORTERM | OYJL_FORCE_COLORTERM ); /* show rich text format on non GNU color extension environment */
    oyjlArgsRender( argc, argv, NULL, NULL,NULL, debug, ui, myMain );
#else
    fprintf( stderr, "No render support compiled in. For a GUI you might by able to use -X json+command and load into oyjl-args-render viewer.\n" );
#endif
  } else if(ui)
  {
    /* ... working code goes here ... */
    const char * abc = NULL;
    char * abc_global_intro = NULL, * abc_footer = NULL;
    const char * temp_abc_file = NULL; /* intermediate abc file name */
    int i,j, found = 0, index, indexes_n = 0, has_diff = 0, use_abc_title_for_output_fn = 0, size = 0,
        fflags = 0, /* format flags */
        flags = 0; /* flags for --remove, --borderless and --verbose */

    if(remove)
    {
      if(oyjlStringSplitFind( remove, ":", "-extra_abc", OYJL_COMPARE_STARTS_WITH, 0,0,0 ) >= 0)
        flags |= ABC2HTML_REMOVE_M_EXTRA;
      if(oyjlStringSplitFind( remove, ":", "-html", OYJL_COMPARE_STARTS_WITH, 0,0,0 ) >= 0)
        flags |= ABC2HTML_REMOVE_M_HTML;
      if(oyjlStringSplitFind( remove, ":", "extra_abc", OYJL_COMPARE_STARTS_WITH, 0,0,0 ) >= 0)
        flags |= ABC2HTML_REMOVE_EXTRA;
    }
    if(verbose)
        flags |= ABC2HTML_VERBOSE;
    if(verbose > 1)
        flags |= ABC2HTML_VERBOSE2;
    if(verbose > 2)
        flags |= ABC2HTML_VERBOSE3;

    if(format)
    {
      if( strcasecmp(format,"abc") == 0 )
        fflags |= ABC2HTML_ABC | ABC2HTML_MULTIPAGE;
      else
      if( strcasecmp(format,"html") == 0 )
        fflags |= ABC2HTML_HTML | ABC2HTML_MULTIPAGE | ABC2HTML_GRAFIC;
      else
      if( oyjlStringStartsWith(format,"svg",OYJL_COMPARE_CASE) )
      {
        fflags |= ABC2HTML_SVG | ABC2HTML_GRAFIC;
        if( strcasecmp(format,"svgs") == 0 )
          fflags |= ABC2HTML_MULTIPAGE;
      }
      else
      if( strcasecmp(format,"png") == 0 )
        fflags |= ABC2HTML_PNG | ABC2HTML_GRAFIC;
      else
      if( strcasecmp(format,"pdf") == 0 )
        fflags |= ABC2HTML_PDF | ABC2HTML_MULTIPAGE | ABC2HTML_GRAFIC;
      else
      if( strcasecmp(format,"abc2html_abc_script_static") == 0 )
      {
        puts( abc2html_abc_script_static );
        goto clean_main;
      }
      else
      if( strcasecmp(format,"abc2html_css_layout_static") == 0 )
      {
        puts( abc2html_css_layout_static );
        goto clean_main;
      }
    }

    if((fflags & ABC2HTML_GRAFIC || fflags & ABC2HTML_ABC) && !(fflags & ABC2HTML_HTML) && output && strcmp(output,"=") == 0)
      ++use_abc_title_for_output_fn;

    if(output && input && strcmp(output, input) == 0 && !use_abc_title_for_output_fn)
    {
      fprintf( stderr, OYJL_DBG_FORMAT "%s: %s => \"%s\" %s\n", OYJL_DBG_ARGS, oyjlTermColorF(oyjlRED, "%s", _("Error")), input, output, _("Rejected") );
      error = 1;
      goto clean_main;
    }

    if(output && strcmp(output,"=") == 0 && input && input[0] && format && strcasecmp(strrchr(input,'.'),format) != 0)
    {
      char * new_end = oyjlStringToLower( format, 0 );
      output = output_tmp = abc2htmlReplaceFileNameEnding( input, new_end );
      free(new_end);
    }

    count = 0;
    if(list_paths || list_files)
    {
      out = abc2htmlOpenOutput( output, out );
      out_name = output;
    }

    if(input)
    {
      if(strcmp(input,"-") == 0 || strcmp(input,"stdin") == 0)
      {
        ++found;
        text = oyjlReadFileStreamToMem( stdin, &size );
        if(list_paths || list_files)
          fprintf( out, "%s\n", input );
        else
          fprintf( stderr, "%s: stdin %d %s\n", _("Input"), size, _("bytes"));
      }
      else
      {
        /* use a full qualified path */
        char * full_name = oyjlResolveDirFile( input, 0 );
        if(oyjlIsFile( full_name, "r", 0,0,0 ))
          text = oyjlReadFile( full_name, OYJL_IO_STREAM, &size );
        if(!text || !size)
        {
          char ** l = NULL;
          int n = 0, pos = 0;
          /* try in search paths "./" and "~/Music/" with "input" as filter */
          abc2htmlListAllFiles(&l, &n, "abc");
          pos = oyjlStringListFind( l, &n, input, OYJL_COMPARE_FIND_NEEDLE, 0 );
          if(pos < 0)
          {
            input_tmp = oyjlStringCopy( input, 0 );
            oyjlStringPush( &input_tmp, ".abc", 0,0 );
            text = oyjlReadFile( input_tmp, OYJL_IO_STREAM, &size );
            if(text && size)
            {
              ++found;
              input = input_tmp;
              if(list_files)
                fprintf( out, "%s\n", input );
            }
          }
          oyjlStringListRelease( &l, n, free );

          /* try as search path */
          if(!found)
          {
            char * f, * t = NULL;
            int start = 0;
            n = 0;
            if(oyjlIsDir( full_name ))
            {
              listFiles( full_name, &l, &n, "abc" );
              if(verbose)
                fprintf( stderr, OYJL_DBG_FORMAT, OYJL_DBG_ARGS );
              fprintf( stderr, "trying path: %s with %d\n", full_name?full_name:input, n );
            }
            for(i = 0; i < n; ++i)
            {
              size = 0;
              f = l[i];
              t = NULL;
              if(!input || oyjlStringFind(f, full_name, OYJL_COMPARE_LAZY) > 0)
                t = oyjlReadFile( f, OYJL_IO_STREAM, &size );
              if(t && size)
              {
                start = count;
                abc2htmlAddAbc( t, size, &abcs, &count, f, &inputs, flags, &abc_global_intro, &abc_footer );
                oyjlStringPush( &text, t, 0,0 );
                free(t); t = NULL;
                if(list_files)
                  fprintf( out, "%s\n", f );
                if(verbose)
                  fprintf( stderr, OYJL_DBG_FORMAT "%s: %s\t%d %s\t%s: %d [%d]->[%d]\n", OYJL_DBG_ARGS, _("Input"), f, size, _("bytes"), _("Titles"), count-start, start, count-1);
                ++found;
              }
            }
            oyjlStringListRelease( &l, n, free );
          }
        }
        else if(list_files)
          fprintf( out, "%s\n", full_name );
        else if(list_paths)
          fprintf( out, "%s\n", abc2htmlGetPath( full_name ) );

        if(found)
          fprintf( stderr, "%s: %s %d %s\n", _("Input"), input, size, _("bytes"));

        if(full_name) free(full_name);
      }
    }

    /* Use ABC's from paths. See abc2htmlListAllFiles() */
    if(!text)
    {
      char ** l = NULL, * f, * t = NULL;
      int n = 0, abc_add_n = 0, start = 0;
      found = 0;
      abc2htmlListAllFiles(&l, &n, "abc");
      for(i = 0; i < n; ++i)
      {
        size = 0;
        f = l[i];
        if(!input || oyjlStringFind(f, input, OYJL_COMPARE_CASE | OYJL_COMPARE_STARTS_WITH) > 0)
          t = oyjlReadFile( f, OYJL_IO_STREAM, &size );
        if(t && size)
        {
          start = abc_add_n;
          abc2htmlAddAbc( t, size, &abcs, &count, f, &inputs, flags, &abc_global_intro, &abc_footer );
          oyjlStringPush( &text, t, 0,0 );
          free(t); t = NULL;
          if(list_files)
            fprintf( out, "%s\n", f );
          if(verbose)
            fprintf( stderr, OYJL_DBG_FORMAT "%s: %s\t%d %s\t%s: %d [%d]->[%d]\n", OYJL_DBG_ARGS, _("Input"), f, size, _("bytes"), _("Titles"), count-start, start, count-1);
          ++found;
          abc_add_n = count;
        }
      }
      input = input_tmp = oyjlStringCopy(l[i-1], 0);
      oyjlStringListRelease( &l, n, free );
      fprintf( stderr, "%s: %d %s", _("Input"), found, _("Files"));
      fprintf( stderr, " %d %s\n", text?(int)strlen(text):0, _("bytes"));
    }

    if(list_paths)
    {
      char ** l = NULL, * t;
      int n = 0;
      for(i = 0 ; i < count; ++i)
      {
        t = inputs[i];
        oyjlStringListPush( &l, &n, abc2htmlGetPath( t ), 0,0 );
      }
      oyjlStringListFreeDoubles( l, &n, free );
      for(i = 0; i < n; ++i)
        fprintf( out, "%s\n", l[i] );
      oyjlStringListRelease( &l, n, free );
    }

    if(list_paths || list_files)
      goto clean_main;

    if(fflags & ABC2HTML_SVG || fflags & ABC2HTML_PDF || fflags & ABC2HTML_PNG || (fflags & ABC2HTML_ABC && use_abc_title_for_output_fn))
    {
      temp_abc_file = "abc2html.temp.abc";
      out_name = temp_abc_file;
      out = oyjlFopen( out_name, "w" );
      if(verbose)
        fprintf( stderr, "Intermediate %s: %s\n", _("Output"), temp_abc_file );
    }

    if(!output || (strcmp(output,"-") == 0 || strcmp(output,"stdout") == 0))
    {
      if(!out)
        out = stdout;
      fprintf( stderr, "%s: stdout\n", _("Output"));
      out_name = "stdout";
    }
    else
    {
      if(!out)
      {
        out_name = output;
        out = oyjlFopen( out_name, "w" );
      }
      fprintf( stderr, "%s: %s\n", _("Output"), output);
    }

    if(text && !abcs)
      abc2htmlAddAbc( text, size, &abcs, &count, input, NULL, flags, &abc_global_intro, &abc_footer );

    if(count)
    {
      indexes_n = count;
      indexes = calloc( sizeof(int), indexes_n );
      for(i = 0; i < count; ++i) indexes[i] = i;
      if(filter || select >= 0.0)
        indexes_n = abc2htmlFilterAbcs( abcs, count, filter, (int)select, indexes, verbose );
    } else
    {
      if(fflags & ABC2HTML_ABC && abc_footer && select <= 0.0)
      {
        fputs( abc_footer, out );
        free(abc_footer); abc_footer = NULL;
      }
      else
        fprintf( stderr, "%s \"%s\"\n", _("Found No MusicABC tunes in"), input?input:"----" );
      goto clean_main;
    }

    if(list)
    {
      const char * old_input = input;
      if(count && abc2htmlIsAbc( abcs[0] ) == 0)
        abcs[0][0] = '_';

      fprintf( stderr, "%s: %s\n", _("Titles"), oyjlTermColorF( oyjlGREEN, "%d", count ) );
      if(verbose > 1)
        fprintf( stderr, OYJL_DBG_FORMAT "indexes_n:%d count: %d\n", OYJL_DBG_ARGS, indexes_n, count );

      if(list[0] && strcmp(list,"1") != 0)
      {
        char * t = NULL;
        char * h = NULL;
        int header = oyjlStringSplitFind(list, ":", "header", OYJL_COMPARE_STARTS_WITH, &h, 0,0) >= 0;
        int header_tags_n = 0;
        char ** header_tags = oyjlStringSplit2( h && strlen(h) > 7 ? &h[7] : h, ",", NULL, &header_tags_n, NULL, NULL );
        int one_line = h ? oyjlStringSplitFind(h, ",", "1", OYJL_COMPARE_EXACT, NULL, 0,0) >= 0 : 0;
        if(one_line) fprintf(stderr, "use one_line: header:1\n" );
        if(verbose)
          for(i = 0; i < header_tags_n; ++i)
            fprintf( stderr, OYJL_DBG_FORMAT "[%d]: %s\n", OYJL_DBG_ARGS, i, header_tags[i] );
        if(header)
        {
          fprintf( stderr, "%s: %d/%d\n\n", _("Select"), indexes_n, count );
          for(i = 0; i < indexes_n; ++i)
          {
            int lines_n = 0;
            char ** lines;

            index = indexes[i];
            if(inputs && strcmp(old_input, inputs[index]) != 0)
            {
              fprintf( stderr, "%s: %s\n", _("Input"), oyjlTermColor(oyjlITALIC,inputs[index]) );
              old_input = inputs[index];
            }

            abc = abcs[index];
            lines = oyjlStringSplit2( abc, "\n", NULL, &lines_n, NULL, NULL );
            for(j = 0; j < header_tags_n; ++j)
            {
              const char * ht = header_tags[j];
              char * tag = oyjlStringCopy( ht, 0 ),
                   * start = NULL;
              int line_pos;
              char * line = NULL;
              t = strchr(tag,'=');
              if(t != NULL)
              {
                t[0] = '\000';
              }
              oyjlStringAdd( &start, 0,0, "%s:", tag );
              line_pos = oyjlStringListFind(lines, &lines_n, start, OYJL_COMPARE_STARTS_WITH, 0);
              if(line_pos >= 0)
                line = lines[line_pos];
              if(line)
                fprintf( out, "%s%s%s", j == 0?oyjlTermColorF( oyjlNO_MARK, "[%d]\t", index):"\t", lines[line_pos], one_line?"\t":"\n" );
              else if(verbose && !oyjlStringFind(start, "1", OYJL_COMPARE_STARTS_WITH))
                fprintf(stderr, OYJL_DBG_FORMAT "header line not found: %s\n", OYJL_DBG_ARGS, start );
              if(tag) free(tag);
              if(start) free(start);
            }
            fprintf( out, "\n" );
            oyjlStringListRelease( &lines, lines_n, free );
          }
        }
        oyjlStringListRelease( &header_tags, header_tags_n, free );
        if(h) free(h);
      }
      else
      {
        for(i = 0; i < indexes_n; ++i)
        {
          int lines_n = 0;
          char ** lines, * line = NULL;
          found = 0;
          index = indexes[i];
          abc = abcs[index];
          lines = oyjlStringSplit2( abc, "\n", NULL, &lines_n, NULL, NULL );
          if(inputs && strcmp(old_input, inputs[index]) != 0)
          {
            fprintf( stderr, "%s: %s\n", _("Input"), oyjlTermColor(oyjlITALIC,inputs[index]) );
            old_input = inputs[index];
          }
          for(j = 0; j < lines_n; ++j)
          {
            line = lines[j];
            if(oyjlStringFind(line, "T:", OYJL_COMPARE_STARTS_WITH) == 1)
            {
              if(found)
                fprintf( out, "\t  " );
              else
                fprintf( out, "[%d]\t", index );
              ++found;
              fprintf( out, "%s\n", line );
            }
          }
          oyjlStringListRelease( &lines, lines_n, free );
        }
      }

      /* print found position numbers */
      if(indexes_n && count != indexes_n)
      {
        found = 0;
        fprintf( out, "[ " );
        for(i = 0; i < indexes_n; ++i)
        {
          index = indexes[i];
          if(indexes[i] && abc2htmlIsAbc( abcs[index] ) )
          {
            if(found)
              fprintf( out, ", " );
            fprintf( out, "%d", index );
            ++found;
          }
        }
        fprintf( out, " ]\n" );
      }
    }

    if(format)
    {
      int pos = 1,
          first_lyric_only = 0, pid = 0;
      if( fflags )
      {
        char * txt = NULL;
        const char * abc_script = NULL;
        const char * soundfont = NULL;
        const char * pagescript = NULL;
        const char * css_layout = NULL;
        char * css_layout_tmp = NULL;
        const char * html_in = NULL;
        const char * html_out = NULL;
        const char * html_footer = NULL;
        char * abc_in_tmp = NULL,
             * abc_ifnot_grid2_tmp = NULL,
             * abc_if_grid2_tmp = NULL,
             * abc_priority_tmp = NULL,
             * style_tmp = NULL;
        const char * lyric_def = NULL;
        const char * abc_borderless = NULL;
        int abc2page_flags = 0;
        char * style_abc_intro = NULL,
             * style_css_layout2 = NULL,
             * style_abc_if_grid2 = NULL,
             * style_abc_ifnot_grid2 = NULL,
             * style_width = NULL;

        if(count && fflags & ABC2HTML_ABC )
          fprintf( out, "\n" );

        if( style )
        {
          int status = 0, size = 0;
          char * a2hstyle = oyjlReadFile( style, OYJL_IO_STREAM | OYJL_IO_RESOLVE, &size );
          oyjl_val root, v;
          const char * t;

          if(!a2hstyle || !size)
          {
            char ** l = NULL;
            int n = 0, pos = 0;
            /* try in search paths "./" and "~/Music/" with "style" as filter */
            abc2htmlListAllFiles(&l, &n, "a2hstyle");
            pos = oyjlStringListFind( l, &n, style, OYJL_COMPARE_FIND_NEEDLE, 0 );
            if(pos >= 0 && pos < n)
            {
              style_tmp = oyjlStringCopy( l[pos], 0 );
              a2hstyle = oyjlReadFile( style_tmp, OYJL_IO_RESOLVE, &size );
              if(a2hstyle && size)
              {
                ++found;
                style = style_tmp;
              }
            }
            if(verbose) 
              fprintf( stderr, "%s: %s[%d] from %d -> %s %s\n", _("Style"), oyjlTermColor(oyjlITALIC,style), pos, n, style, found?"found":"failed" );
            oyjlStringListRelease( &l, n, free );
          }
          root = oyjlTreeParse2( a2hstyle, 0, __func__, &status );
          v = oyjlTreeGetValue( root, 0, "abc2html-style1/abc/intro" );
          t = OYJL_GET_STRING(v);
          t = oyjlJsonEscape( t, OYJL_REVERSE );
          style_abc_intro = oyjlStringCopy( t,0 );
          v = oyjlTreeGetValue( root, 0, "abc2html-style1/abc/if_grid2" );
          t = OYJL_GET_STRING(v);
          t = oyjlJsonEscape( t, OYJL_REVERSE );
          style_abc_if_grid2 = oyjlStringCopy( t,0 );
          v = oyjlTreeGetValue( root, 0, "abc2html-style1/abc/ifnot_grid2" );
          t = OYJL_GET_STRING(v);
          t = oyjlJsonEscape( t, OYJL_REVERSE );
          style_abc_ifnot_grid2 = oyjlStringCopy( t,0 );
          v = oyjlTreeGetValue( root, 0, "abc2html-style1/css/layout2" );
          t = OYJL_GET_STRING(v);
          t = oyjlJsonEscape( t, OYJL_REVERSE );
          style_css_layout2 = oyjlStringCopy( t,0 );
          v = oyjlTreeGetValue( root, 0, "abc2html-style1/width" );
          t = OYJL_GET_STRING(v);
          t = oyjlJsonEscape( t, OYJL_REVERSE );
          style_width = oyjlStringCopy( t,0 );
          if(verbose)
            fprintf( stderr, "%s style: \"%s\" %d abc2html-style1/abc/intro:%d abc/if_grid2:%d abc/ifnot_grid2:%d abc2html-style1/css/layout2:%d %s\n", _("Read"), oyjlTermColor(oyjlITALIC,style), size, style_abc_intro?(int)strlen(style_abc_intro):0, style_abc_if_grid2?(int)strlen(style_abc_if_grid2):0, style_abc_ifnot_grid2?(int)strlen(style_abc_ifnot_grid2):0, style_css_layout2?(int)strlen(style_css_layout2):0, style_width?style_width:"" );
          oyjlTreeFree( root );
        }

        if( fflags )
        {
          abc_intro = abc2htmlReadFile( abc_intro, style_abc_intro, style, abc2html_abc_intro_static, &abc_in_tmp, "abc_intro", fflags, verbose );
          ifnot_grid2 = abc2htmlReadFile( ifnot_grid2, style_abc_ifnot_grid2, style, NULL, &abc_ifnot_grid2_tmp, "ifnot_grid2", fflags, verbose );
          if_grid2 = abc2htmlReadFile( if_grid2, style_abc_if_grid2, style, NULL, &abc_if_grid2_tmp, "if_grid2", fflags, verbose );
          abc_priority = abc2htmlReadFile( abc_priority, NULL, style, NULL, &abc_priority_tmp, "abc_priority", fflags, verbose );
        }

        /* Header */
        if(fflags & ABC2HTML_HTML && count)
        {
          const char * t = title;
          if(!html_in)
            html_in = abc2html_html_in_static;
          if(!html_out)
            html_out = abc2html_html_out_static;
          if(!abc_script)
            abc_script = abc2html_abc_script_static;
          if(!soundfont)
            soundfont = abc2html_soundfont_static;
          if(!pagescript)
            pagescript = abc2html_pagescript_static;
          if(!css_layout)
            css_layout = abc2html_css_layout_static;
          if(css_layout2)
          {
            int size = 0;
            if(verbose)
              fprintf( stderr, "%s css_layout2: \"%s\" ", _("Read"), oyjlTermColor(oyjlITALIC,css_layout2) );
            css_layout2 = css_layout_tmp = oyjlReadFile( css_layout2, OYJL_IO_RESOLVE | OYJL_IO_STREAM, &size );
            if(verbose && size)
              fprintf( stderr, " %d byte\n", size );
          }
          else if(style_css_layout2)
          {
            css_layout2 = style_css_layout2;
            if(verbose)
              fprintf( stderr, "%s css_layout2 <- \"%s\"\n", _("Use"), oyjlTermColor(oyjlITALIC,style) );
          }
          else
          {
            css_layout2 = abc2html_css_layout2_static;
            if(verbose)
              fprintf( stderr, "%s static css_layout2\n", _("Use") );
          }
          if(!html_footer)
            html_footer = abc2html_html_footer_static;

          if(!title)
          {
            char * t2 = NULL;
            if(output)
              txt = oyjlStringCopy( output, 0 );
            else
              txt = oyjlStringCopy( input, 0 );
            if(txt)
              t2 = strrchr( txt, '.' );
            if(t2) t2[0] = '\000';
            if(txt)
            {
              oyjlStringReplace( &txt, ".", " ", 0,0 );
              t = txt;
            }
            t2 = txt;
            if(t2) t2[0] = toupper(t2[0]);
            while( t2 && t2[0] && ((t2 = strstr( t2, " " )) != NULL) )
            { t2[1] = toupper(t2[1]); ++t2; }
          }

          if(html_in && html_in[0])
          {
            fprintf( out, html_in, t );
            fprintf( out, "  <!-- abc2html section end: html_in --->\n" );
          }
          if(abc_script && abc_script[0])
          {
            fprintf( out, "  <script>\n" );
            fprintf( out, "    <!-- abc2html section start: abc_script --->\n" );
            fprintf( out, "    %s\n", abc_script );
            fprintf( out, "    <!-- abc2html section end: abc_script --->\n" );
            fprintf( out, "  </script>\n" );
          }
          if(soundfont && soundfont[0])
          {
            fprintf( out, "  <script>\n" );
            fprintf( out, "    if (typeof(MIDI) === 'undefined') var MIDI = {};\n");
            fprintf( out, "    if (typeof(MIDI.Soundfont) === 'undefined') MIDI.Soundfont = {};\n");
            fprintf( out, "    MIDI.Soundfont.acoustic_grand_piano = {\n");
            fprintf( out, "    <!-- abc2html section start: soundfont --->\n" );
            fprintf( out, "    %s\n", soundfont );
            fprintf( out, "    <!-- abc2html section end: soundfont --->\n" );
            fprintf( out, "  </script>\n" );
          }
          if(pagescript && pagescript[0])
          {
            fprintf( out, "  <script>\n" );
            fprintf( out, "    <!-- abc2html section start: pagescript --->\n" );
            fprintf( out, "    %s\n", pagescript );
            fprintf( out, "    <!-- abc2html section end: pagescript --->\n" );
            fprintf( out, "  </script>\n" );
          }
          if(css_layout && css_layout[0])
          {
            fprintf( out, "    <!-- abc2html section start: css_layout --->\n" );
            fprintf( out, "  <style>\n" );
            fprintf( out, "    %s\n", css_layout );
            fprintf( out, "  </style>\n" );
            fprintf( out, "    <!-- abc2html section end: css_layout --->\n" );
          }
          if(css_layout2 && css_layout2[0])
          {
            fprintf( out, "    <!-- abc2html section start: css_layout2 --->\n" );
            fprintf( out, "  <style>\n" );
            fprintf( out, "    %s\n", css_layout2 );
            fprintf( out, "  </style>\n" );
            fprintf( out, "    <!-- abc2html section end: css_layout2 --->\n" );
          }
          if(css_layout_tmp) { free(css_layout_tmp); css_layout_tmp = NULL; }

          if(html_out && html_out[0])
          {
            fprintf( out, "    <!-- abc2html section start: html_out --->\n" );
            fprintf( out, html_out, t );
            fprintf( out, "    <!-- abc2html section end: html_out --->\n" );
          }
          if(txt) free(txt);
        }

        if(abc_global_intro && !(select && select > indexes_n)) /* place global stuff only once into ABC output */
          fputs( abc_global_intro, out );

        for(i = 0; i < indexes_n; ++i)
        {
          int abc_start = 0, abc_end = -1;
          index = indexes[i];
          abc = abcs[index];
          while( abc && abc[0] && isspace(abc[0]) ) abc++;
          if(abc2htmlIsAbc(abc))
          {
            int lines_n = 0,
                has_X = 1;
            char ** lines;
            char * line, * text, * temp = NULL;
            int images_n = 0;
            char ** images = NULL;
            if(pos > 1 && !(fflags & ABC2HTML_MULTIPAGE)) break; 
            if(pos == 1 && oyjlStringFind( abc, "%%grid2", OYJL_COMPARE_FIND_NEEDLE ) == 1) first_lyric_only = 1;

            lines = oyjlStringSplit2( abc, "\n", NULL, &lines_n, NULL, NULL );
            abc_end = lines_n;

            /* check for proper X: header field */
            if( oyjlStringFind( lines[0], "X:", OYJL_COMPARE_STARTS_WITH ) <= 0 &&
                oyjlStringFind( abc, "\nX:", OYJL_COMPARE_FIND_NEEDLE ) <= 0 )
              has_X = 0;

            if(fflags & ABC2HTML_ABC || fflags & ABC2HTML_GRAFIC)
            {
              if(oyjlStringFind( abc, "%%grid2", OYJL_COMPARE_FIND_NEEDLE ) == 1)
                lyric_def = if_grid2 ? if_grid2:"";
              else
                lyric_def = ifnot_grid2 ? ifnot_grid2:"";
            }

            if( borderless &&
                ( fflags & ABC2HTML_ABC || fflags & ABC2HTML_GRAFIC)
              )
              abc_borderless = "%%topmargin 0.1cm\n%%botmargin 0.1cm\n%%leftmargin 0.1cm\n%%rightmargin 0.1cm\n";

            abc2htmlGetAbcRange( abc, &abc_start, &abc_end, flags );

            if(!(fflags & ABC2HTML_HTML))
              fprintf( out, "\n" );

            if(abc_intro && abc_intro[0])
            {
              int lout_n = 0;
              char ** lout;
              if(fflags & ABC2HTML_HTML)
              {
                fprintf( out, "\n" );
                fprintf( out, "    <!-- abc2html section start: abc-automatic abc_intro --->\n" );
                //fprintf( out, "    <div class=\"abc-tune\">\n" ); Idea to css style images together with abc. Is not compatible with the page script.
                fprintf( out, "      <div class=\"abc-source 26 1.4\">" );
              }
              fprintf( out, "\n%s%%%%pagewidth %s\n", abc_intro, style_width ? style_width : width );
              if(fflags & ABC2HTML_HTML)
                fprintf( out, "%%%%abc2html section end:   abc-automatic abc_intro\n" );
              lout = oyjlStringSplit2( lyric_def, "\n", NULL, &lout_n, NULL, NULL );
              if(lout_n)
                fprintf( out, "%%%%abc2html section start: abc-automatic abc_lyric\n" );
              for( j = 0; j < lout_n; ++j )
              {
                line = lout[j];
                int lout_pos = oyjlStringListFind(lines, &lines_n, line, OYJL_COMPARE_STARTS_WITH, 0);
                if(lout_pos < 0) /* skip already included lines, abc_intro should be safe */
                  fprintf( out, "%s%s", j?"\n":"", lout[j] );
              }
              if(lout_n)
                fprintf( out, "%%%%abc2html section end:   abc-automatic abc_lyric\n" );
              oyjlStringListRelease( &lout, lout_n, free );
            }

            if(abc_borderless)
            {
              fprintf( out, "%%%%abc2html section start: abc-automatic abc_boderless\n" );
              fprintf( out, "%s", abc_borderless );
              fprintf( out, "%%%%abc2html section end:   abc-automatic abc_boderless\n" );
            }

            if(abc_priority)
            {
              fprintf( out, "%%%%abc2html section start: abc-automatic abc_priority\n" );
              fprintf( out, "%s", abc_priority );
              fprintf( out, "%%%%abc2html section end:   abc-automatic abc_priority\n" );
            }

            for(j = abc_start; j < abc_end; ++j)
            {
              int show = 1;
              line = lines[j];
              text = line;
              //while( text && text[0] && isspace(text[0]) ) text++;
              if( flags & ABC2HTML_REMOVE_EXTRA &&
                  ( oyjlStringFind( text, "%%MIDI", OYJL_COMPARE_STARTS_WITH ) <= 0 &&
                    oyjlStringFind( text, "%%capo", OYJL_COMPARE_STARTS_WITH ) <= 0 &&
                    oyjlStringFind( text, "%%diagram", OYJL_COMPARE_STARTS_WITH ) <= 0 &&
                    oyjlStringFind( text, "%%equalbars", OYJL_COMPARE_STARTS_WITH ) <= 0 &&
                    oyjlStringFind( text, "%%grid", OYJL_COMPARE_STARTS_WITH ) <= 0 &&
                    oyjlStringFind( text, "%%notespacingfactor", OYJL_COMPARE_STARTS_WITH ) <= 0 &&
                    oyjlStringFind( text, "%%setdiag", OYJL_COMPARE_STARTS_WITH ) <= 0 &&
                    oyjlStringFind( text, "%%tempo", OYJL_COMPARE_STARTS_WITH ) <= 0 &&
                    oyjlStringFind( text, "%%topspace", OYJL_COMPARE_STARTS_WITH ) <= 0 &&
                    oyjlStringFind( text, "%%", OYJL_COMPARE_STARTS_WITH ) > 0 )
                )
                show = 0;
              if( !(flags & ABC2HTML_REMOVE_M_HTML) &&
                  ( text[0] == '<' || /* omit html */
                    oyjlStringFind( text, "$ABC_UI", OYJL_COMPARE_STARTS_WITH ) > 0 || oyjlStringFind( text, "showAuswahlAndButtons", OYJL_COMPARE_STARTS_WITH ) > 0
                  ) /* omit HTML */
                )
                  show = 0;
              if( show )
              {
                if(verbose)
                  fprintf( stderr, "%s", oyjlTermColorF(oyjlGREEN, "[%d]", j ) );

                /* fix missing X: header line */
                if(has_X == 0 && j == abc_start)
                {
                  if(oyjlStringFind( text, "%abc", OYJL_COMPARE_STARTS_WITH ) > 0)
                  {
                    fprintf( out, "\n%s\n", line );
                    fprintf( out, "X:%d\n", pos++ );
                    fprintf( stderr, OYJL_DBG_FORMAT "%s: %s[%d]  %s\n", OYJL_DBG_ARGS, oyjlTermColorF(oyjlBLUE, "%s", _("Warning")), inputs?inputs[index]:input, index, _("Fix missing X: header line") );
                    text = NULL;
                    continue;
                  }
                  else
                  {
                    fprintf( stderr, OYJL_DBG_FORMAT "%s: %s[%d]  %s  \"%s\"\n", OYJL_DBG_ARGS, oyjlTermColorF(oyjlBLUE, "%s", _("Warning")), inputs?inputs[index]:input, index, _("Fix missing X: header line"), text );
                    fprintf( out, "X:%d\n", pos++ );
                  }
                }

                if(oyjlStringFind( text, "X:", OYJL_COMPARE_STARTS_WITH ) > 0)
                  fprintf( out, "X:%d\n", pos++ );
                else
                {
                  int is_k_line = 0,
                      is_f_line = 0,
                      is_W_line = 0;
                  char * txt = oyjlStringCopy( text, 0 );
                  if(fflags & ABC2HTML_HTML)
                    is_k_line = oyjlStringFind( text, "K:", OYJL_COMPARE_STARTS_WITH ) > 0;
                  if(fflags & ABC2HTML_HTML || fflags & ABC2HTML_PDF)
                    is_f_line = oyjlStringFind( text, "F:", OYJL_COMPARE_STARTS_WITH ) > 0;
                  is_W_line = oyjlStringFind( text, "W:", OYJL_COMPARE_STARTS_WITH ) > 0;

                  if(is_k_line) /* musical key */
                  {
                    oyjlStringReplace( &txt, "K: ", "K:", 0,0 );
                    oyjlRegExpReplace( &txt, "maj$", "" );
                    oyjlRegExpReplace( &txt, "mix$", "" );
                    oyjlRegExpReplace( &txt, "min$", "m" );
                    oyjlRegExpReplace( &txt, "Cmix$", "F" );
                    oyjlRegExpReplace( &txt, "Ddor$", "C" );
                    oyjlRegExpReplace( &txt, "Gdor$", "F" );
                    oyjlRegExpReplace( &txt, "Am$", "C" );
                    oyjlRegExpReplace( &txt, "Cm$", "Eb" );
                    oyjlRegExpReplace( &txt, "Dm$", "F" );
                    oyjlRegExpReplace( &txt, "Em$", "G" );
                    if(strcmp(txt, line) != 0)
                      fprintf( stderr, "changed[%d]: \"%s\" -> \"%s\"\n", i, line, txt );
                  }
                  else
                    if(is_f_line)
                  {
                    char * fileext = strrchr(txt, '/');
                    fileext = strrchr(fileext?fileext:txt,'.');
                    if(verbose)
                      fprintf( stderr, OYJL_DBG_FORMAT "found F: %s \"%s\" %s\n", OYJL_DBG_ARGS, txt, line, fileext );
                    if(fileext)
                    {
                      ++fileext;
                      if( oyjlStringFind( fileext, "svg", OYJL_COMPARE_CASE ) ||
                          oyjlStringFind( fileext, "png", OYJL_COMPARE_CASE ) ||
                          oyjlStringFind( fileext, "jpg", OYJL_COMPARE_CASE )
                        )
                      {
                        oyjlStringReplace( &txt, "F: ", "F:", 0,0 );
                        oyjlStringReplace( &txt, "F:", "", 0,0 );
                        oyjlStringListPush( &images, &images_n, txt, 0,0 );
                        fprintf( stderr, OYJL_DBG_FORMAT "found F: %s \"%s\"\n", OYJL_DBG_ARGS, txt, line );
                        oyjlStringPrepend( &txt, "K:", 0,0 );
                      }
                      else if(fflags & ABC2HTML_HTML)
                        oyjlStringReplace( &txt, "<", "&lt;", 0,0 );
                    }
                  }
                  else if(fflags & ABC2HTML_HTML)
                    oyjlStringReplace( &txt, "<", "&lt;", 0,0 );
                  if(is_W_line)
                    oyjlStringReplace( &txt, "  ", "  ", 0,0 );

                  /* fix empty lines */
                  if(txt && txt[0])
                    fprintf( out, "%s\n", txt );
                  else
                    fprintf( stderr, OYJL_DBG_FORMAT "%s: %s[%d]:%d \"%s\"\n", OYJL_DBG_ARGS, oyjlTermColorF(oyjlRED, "%s", _("Error")), inputs?inputs[i]:input, i, j, line );

                  if(txt) { free(txt); txt = NULL; }
                }
              }
              text = NULL;
            }

            if(fflags & ABC2HTML_HTML)
              fprintf( out, "%s\n", "      </div>" );

            if(verbose)
              fprintf( stderr, OYJL_DBG_FORMAT "images_n: %d\" />\n", OYJL_DBG_ARGS, images_n );
            if(images_n)
            {
              int k;
              for(k = 0; k < images_n; ++k)
              {
                const char * image = images[k];
                int size = 0;
                char * mem = NULL;
                char * data;
                const char * fileext = strrchr(image,'.') + 1,
                           * mime = NULL;

                if(fflags & ABC2HTML_HTML)
                {
                  mem = oyjlReadFile( image, 0, &size );
                  if(!mem || size < 4)
                  {
                    fprintf( stderr, OYJL_DBG_FORMAT "F:%s not readable\n", OYJL_DBG_ARGS, image );
                    if(mem) free(mem);
                    continue;
                  }
                } else
                  size = oyjlIsFile( image, "r", 0, NULL, 0 );

                     if( oyjlStringFind( fileext, "png", OYJL_COMPARE_CASE ) ) mime = "image/png";
                else if( oyjlStringFind( fileext, "jpg", OYJL_COMPARE_CASE ) ) mime = "image/jpeg";

                if(verbose)
                  fprintf( stderr, OYJL_DBG_FORMAT "F:%s fileext:%s size: %d mime: %s\n", OYJL_DBG_ARGS, image, fileext, size, mime?mime:"----" );
                if(fflags & ABC2HTML_HTML)
                {
                  if(mime)
                  {
                    int w = abc2htmlSizetoPixel( style_width ? style_width : width, 96.0, verbose );
                    data = abc2htmlBas64Encode( (const unsigned char *) mem, size, verbose );
                    fprintf( out, "      <img style=\"width:%dpx\" src=\"data:%s;base64,%s\" />\n", w, mime, data );
                    free(data);
                  }
                  else
                    fprintf( out, "      %s\n", mem );

                  if(k+1 < images_n)
                    fprintf( out, "      <br />\n" );
                }
                if(mem) free(mem);
              }
              oyjlStringListRelease( &images, images_n, 0 );
            }

            //if(fflags & ABC2HTML_HTML) fprintf( out, "%s\n", "    </div>" );

            oyjlStringListRelease( &lines, lines_n, free );
            if(verbose > 1)
              fprintf( stderr, OYJL_DBG_FORMAT "%% -------------%d -> %d -----------------\n", OYJL_DBG_ARGS, abc_start, abc_end );
            if(temp) {free(temp); temp = NULL;}
          }
        }

        if(html_footer && html_footer[0])
        {
          const char * date = oyjlPrintTime(OYJL_DATE, oyjlNO_MARK);
          int i, n = 0;
          char ** results = oyjlOptions_ResultsToList( ui->opts, NULL, &n );
          char * t = NULL;
          oyjlStringPush( &t, "<br />abc2html", 0,0 );
          for(i = 0; i < n; ++i) oyjlStringAdd( &t, 0,0, " %s", results[i] );
          oyjlStringListRelease( &results, n, free );
          oyjlStringPush( &t, "\n", 0,0 );
          fprintf( out, "    <!-- abc2html section start: html_footer --->\n" );
          fprintf( out, html_footer, date, t );
          fprintf( out, "    <!-- abc2html section end: html_footer --->\n" );
          free(t);
        }

        if(out && out != stdout)
        {
          fprintf( stderr, "%s %s %s %s abc's:%d\n", _("wrote"), oyjlTermColorF(oyjlITALIC, "%d", oyjlFsize(out)), _("to"), oyjlTermColor(oyjlBOLD,out_name?out_name:"----"), pos - 1 );
          fclose( out ); out = NULL;
        }

        if(first_lyric_only) abc2page_flags |= ABC2HTML_FIRST_LYRIC_ONLY;
        if(borderless)       abc2page_flags |= ABC2HTML_BORDERLESS;
        if(verbose)          abc2page_flags |= ABC2HTML_VERBOSE;

        if(fflags & ABC2HTML_SVG)
        {
          char ** files;
          int n = 0;

          files = abc2htmlCreateSvg( temp_abc_file, pos-1, output, ui, abc2page_flags );

          while(files && files[n]) ++n;

          if(!output || strcmp(output,"stdout") == 0 || strcmp(output,"-") == 0)
          {
            for(i = 0; i < n; ++i)
            {
              char * file = files[i];
              int size = 0;
              char * text = oyjlReadFile( files[i], 0, &size );
              fprintf(stderr, "%s %d %s\n", _("Generated"), size, _("bytes") );
              /* erase temporary svg file */
              oyjlWriteFile( file, NULL, 0 );
              fputs( text, stdout );
              if(text) { free(text); text = NULL; }
            }
          }
          else
          {
            if(files && files[0])
            {
              if(use_abc_title_for_output_fn)
              {
                abc2htmlRenameFileToTitle( temp_abc_file, (const char **)files, NULL, "svg", fflags, flags );
              } else
              if(!(fflags & ABC2HTML_MULTIPAGE))
              {
                rename(files[0], output);
                fprintf(stderr, "%s %s (%d %s)\n", _("Generated"), output, abc2htmlGetFileSize(output), _("bytes") );
              }
            } else
            {
              fprintf(stderr, "%s ----\n", _("Generated") );
            }
            if(fflags & ABC2HTML_MULTIPAGE && !use_abc_title_for_output_fn)
              for(i = 0; i < n; ++i)
                fprintf(stderr, "%s %s (%d %s)\n", _("Generated"), files[i], abc2htmlGetFileSize(files[i]), _("bytes") );
          }
          oyjlStringListRelease( &files, n, 0 );
        }

        if(fflags & ABC2HTML_PDF)
        {
          char ** svgs = abc2htmlCreateSvg( temp_abc_file, pos-1, NULL, ui, abc2page_flags );
          int svgs_n = 0;
          const char * target = "abc2html.temp.pdf",
                     * toc_pdf = output;
          char * data;
          char ** images = NULL, * t;
          int images_n = 0;

          while(svgs && svgs[svgs_n]) ++svgs_n;
          while((t = abc2htmlGetAbcField( abc, "F:", images_n )) != NULL && images_n < svgs_n)
          {
            oyjlStringListPush( &images, &images_n, t, 0,0 );
            if(verbose) fprintf(stderr, "%s ", t);
            free(t);
          }

          if(use_abc_title_for_output_fn)
            abc2page_flags |= ABC2PDF_KEEP_INTERMEDIATE;

          data = abc2htmlCreatePdf( (const char **)svgs, svgs_n, target, ui, abc2page_flags );

          if(use_abc_title_for_output_fn)
            abc2htmlRenameFileToTitle( temp_abc_file, (const char **)svgs, "pdf", "pdf", fflags, flags );
          else if(data)
          {
            int size = 0, pos = 1;
            char * t, * text = oyjlReadFile( data, 0, &size );
            oyjlStringReplace( &text, ",", "", 0,0 ); /* fix: dimension of 1,000.04 is read as 1.0 */
            for(i = 0; i < indexes_n; i++)
            {
              char * bookmark = NULL, * T, * R;
              int images_n = 0;
              index = indexes[i];
              abc = abcs[index];
              T = abc2htmlGetAbcField( abc, "T:", 0 );
              R = abc2htmlGetAbcField( abc, "R:", 0 );
              if(T)
                oyjlStringAdd( &bookmark, 0,0, "%s (%s)", T+2+(T[2]==' '?1:0), R?R+2+(R[2]==' '?1:0):"----" );
              if(verbose && bookmark)
                fprintf( stderr, "[%d]: %s images%d\n", i, bookmark, images_n );
              if(!bookmark) continue;
              oyjlStringAdd( &text, 0,0, "BookmarkBegin\n" );
              oyjlStringAdd( &text, 0,0, "BookmarkTitle: %s\n", bookmark );
              oyjlStringAdd( &text, 0,0, "BookmarkLevel: 1\n" );
              oyjlStringAdd( &text, 0,0, "BookmarkPageNumber: %d\n", pos++ );
              if(T) free(T);
              if(R) free(R);
              if(bookmark) free(bookmark);
            }
            /*fprintf( stderr, "%s\n", text );*/
            oyjlWriteFile( data, text, strlen(text) );
            if(!output || strcmp(output,"stdout") == 0 || strcmp(output,"-") == 0)
              toc_pdf = "abc2html.temp.toc.pdf";
            t = oyjlReadCommandF( &size, verbose?"r.verbose":"r", malloc, ABC2PDF2 " %s update_info %s output %s", target, data, toc_pdf );
            if(t) { free(t); t = NULL; }
            size = 0;
            t = oyjlReadFile( temp_abc_file, 0, &size );
            abc2htmlEmbedAbcCodeIntoPDF( output, t, flags );
            if(t) { free(t); t = NULL; }
            if(text) free(text);
          }
          for(i = 0; i < svgs_n; ++i)
          {
            char * file = svgs[i];
            if(!verbose)
              /* erase temporary svg files */
              oyjlWriteFile( file, NULL, 0 );
          }

          if(!data)
            fprintf(stderr, OYJL_DBG_FORMAT "%s:\t%s: %s\n", OYJL_DBG_ARGS, oyjlTermColorF(oyjlRED, "%s", _("Error")), _("Could not create"), target );
          else if(!output || strcmp(output,"stdout") == 0 || strcmp(output,"-") == 0)
          {
            int size = 0;
            char * text = oyjlReadFile( toc_pdf, 0, &size );
            /* erase temporary pdf files */
            oyjlWriteFile( toc_pdf, NULL, 0 );
            fputs( text, stdout );
            if(text) { free(text); text = NULL; }
          }
          if(!verbose)
          {
            oyjlWriteFile( target, NULL, 0 );
            oyjlWriteFile( data, NULL, 0 );
          }
          if(data) free(data);
          oyjlStringListRelease( &svgs, svgs_n, 0 );
        }

        if(fflags & ABC2HTML_PNG)
        {
          int svgs_n = 0, size = 0;
          char ** svgs = abc2htmlCreateSvg( temp_abc_file, pos-1, NULL, ui, abc2page_flags ),
                * abc = oyjlReadFile( temp_abc_file, 0, &size ),
                * pagewidth = abc2htmlGetAbcField( abc, "%%pagewidth", 0 );
          char * data = NULL;

          while(svgs && svgs[svgs_n]) ++svgs_n;

          if(svgs_n)
          {
            double dpi = 300.0;
            int pixel = (int)(21.0 / 2.54 * dpi);
            if(pagewidth)
              pixel = abc2htmlSizetoPixel( pagewidth+strlen("%%pagewidth")+1, dpi, verbose );
            data = abc2htmlCreatePng( svgs[0], pixel, ui, abc2page_flags );
          }
          if(data)
          {
            if(use_abc_title_for_output_fn)
              abc2htmlRenameFileToTitle( temp_abc_file, (const char **)svgs, "png", "png", fflags, flags );
            else
            {
              int size = 0, written_n = 0;
              char * text = oyjlReadFile( data, 0, &size );
              if(!output) output = "stdout";
              written_n = oyjlWriteFile2( output, OYJL_IO_STREAM | OYJL_IO_WRITE | OYJL_IO_RESOLVE, text, size );
              if(verbose)
                fprintf(stderr, OYJL_DBG_FORMAT "Wrote %d to %s\n", OYJL_DBG_ARGS, written_n, output );
              if(text) free(text);
            }
          }
          for(i = 0; i < svgs_n; ++i)
          {
            char * file = svgs[i];
            if(!verbose)
              /* erase temporary svg files */
              oyjlWriteFile( file, NULL, 0 );
          }

          if(!data)
            fprintf(stderr, OYJL_DBG_FORMAT "%s:\t%s: %s\n", OYJL_DBG_ARGS, oyjlTermColorF(oyjlRED, "%s", _("Error")), _("Could not create"), output?output:"PNG" );
          if(!verbose)
            oyjlWriteFile( data, NULL, 0 );
          if(data) free(data);
          oyjlStringListRelease( &svgs, svgs_n, 0 );
        }

        if(fflags & ABC2HTML_ABC && use_abc_title_for_output_fn)
        {
          for(i = 0; i < indexes_n; i++)
          {
            char * fn = NULL, * T;
            index = indexes[i];
            abc = abcs[index];
            T = abc2htmlGetAbcField( abc, "T:", 0 );
            if(T)
              oyjlStringAdd( &fn, 0,0, "%s.abc", T+2+(T[2]==' '?1:0) );
            oyjlWriteFile( fn, abc, strlen(abc) );
            fprintf(stderr, "%s %s (%d %s)\n", _("Generated"), fn, abc2htmlGetFileSize(fn), _("bytes") );
            if(T) free(T);
            if(fn) free(fn);
          }
        }

        /* erase temporary abc file */
        if(temp_abc_file)
        {
          if(verbose)
            fprintf(stderr, "%s Intermediate: %s\n", _("Generated"), temp_abc_file );
          else
            oyjlWriteFile( temp_abc_file, NULL, 0 );
        }

        if(style_abc_intro) free(style_abc_intro);
        if(style_css_layout2) free(style_css_layout2);
        if(style_tmp) free(style_tmp);

      } else
      {
        fprintf( stderr, "Format not (yet) supported: --format=%s\n", oyjlTermColor(oyjlRED,format) );
      }

      if(oyjlHasApplication("diff") && verbose)
        has_diff = 1;
      found = 0;
      for(i = 0; i < indexes_n; ++i)
      {
        char * title = NULL, * title2 = NULL, * t, * t2;
        index = indexes[i];
        if(i == 0) pid = getpid();
        t = strstr( abcs[index], "\nT:" );
        if(t)
        {
          title = oyjlStringCopy( t+1, 0 );
          t2 = strchr( title, '\n' );
          if(t2) t2[0] = '\000';
        }
        for(j = i+1; j < indexes_n; ++j)
        {
          int index2 = indexes[j];
          title2 = NULL;
          t = strstr( abcs[index2], "\nT:" );
          if(t)
          {
            title2 = oyjlStringCopy( t+1, 0 );
            t2 = strchr( title2, '\n' );
            if(t2) t2[0] = '\000';
          }

          if(title && title2 && strcmp( title, title2 ) == 0)
          {
            if(0 == found++)
            {
              fprintf( stdout, "\n" );
              if(verbose && !has_diff)
                fprintf( stdout, "diff %s\n", oyjlTermColor(oyjlRED,_("not available")) );
            }
            if(found && has_diff && verbose)
              fprintf( stdout, "\n" );
            t = NULL;
            fprintf( stdout, "%%%%%s %s \"%s\" %s\n", oyjlTermColorF( oyjlBLUE, "[%d,%d]", index, index2), _("Duplication"), oyjlTermColor(oyjlBOLD,title),
                inputs ? oyjlTermColorFPtr(oyjlITALIC, &t, "\"%s\" \"%s\"", inputs[index], inputs[index2])
                       : oyjlTermColorFPtr(oyjlITALIC, &t, "\"%s\"", input) );
            free(t); t = NULL;
            if(verbose && has_diff)
            {
              char * cmd = NULL;
              t = t2 = NULL;
              oyjlStringAdd( &t, 0,0, "abc2html-tmp-%d-%d.abc", pid, index );
              oyjlWriteFile( t, abcs[index], strlen( abcs[index] ) );
              oyjlStringAdd( &t2, 0,0, "abc2html-tmp-%d-%d.abc", pid, index2 );
              oyjlWriteFile( t2, abcs[index2], strlen( abcs[index2] ) );
              oyjlStringAdd( &cmd, 0,0, "diff -aur --label \"[%4d]:%s\" abc2html-tmp-%d-%d.abc --label \"[%4d]:%s\" abc2html-tmp-%d-%d.abc",
                  index,  oyjlTermColor(oyjlITALIC,inputs ? inputs[index] : input),          pid, index,
                  index2, oyjlTermColorF(oyjlITALIC, "%s", inputs ? inputs[index2] : input), pid, index2 );
              system( cmd );
              oyjlWriteFile( t, NULL, 0 );
              oyjlWriteFile( t2, NULL, 0 );
              free(t); t = NULL;
              free(t2); t2 = NULL;
              free(cmd); cmd = NULL;
            }
          }
          if(title2) free(title2);
        }
        if(title) free(title);
      }
    }

    if(abc_global_intro) {free(abc_global_intro); abc_global_intro = NULL;}
    if(abc_footer) {free(abc_footer); abc_footer = NULL;}
  }
  else error = 1;

  clean_main:
  {
    int i = 0;
    while(oarray[i].type[0])
    {
      if(oarray[i].value_type == oyjlOPTIONTYPE_CHOICE && oarray[i].values.choices.list)
        free(oarray[i].values.choices.list);
      ++i;
    }
    if(text) free(text);
    if(input_tmp) free(input_tmp);
    if(out && out != stdout)
    {
      fprintf( stderr, "%s %s %s %s\n", _("wrote"), oyjlTermColorF(oyjlITALIC, "%d", oyjlFsize(out)), _("to"), oyjlTermColor(oyjlBOLD,output));
      fclose( out ); out = NULL;
    }
  }
  oyjlStringListRelease( &abcs, count, free );
  oyjlStringListRelease( &inputs, count, free );
  if(indexes) free(indexes);
  if(abc2html_get_path_) { free(abc2html_get_path_); abc2html_get_path_ = NULL; }
  oyjlUi_Release( &ui );

  return error;
}

extern int * oyjl_debug;
#ifndef NO_MAIN
int main( int argc_, char**argv_, char ** envv )
{
  int argc = argc_;
  char ** argv = argv_;
  oyjlTranslation_s * trc_ = NULL;
  const char * loc = NULL;
  const char * lang;

#ifdef __ANDROID__
  argv = calloc( argc + 2, sizeof(char*) );
  memcpy( argv, argv_, (argc + 2) * sizeof(char*) );
  argv[argc++] = "--render=gui"; /* start Renderer (e.g. QML) */
  environment = environ;
#else
  environment = envv;
#endif

  /* language needs to be initialised before setup of data structures */
  int use_gettext = 0;
#ifdef OYJL_HAVE_LIBINTL_H
  use_gettext = 1;
#endif
#ifdef OYJL_HAVE_LOCALE_H
  loc = oyjlSetLocale(LC_ALL,"");
#endif
  lang = getenv("LANG");
  if(!loc)
  {
    loc = lang;
    fprintf( stderr, "%s", oyjlTermColor(oyjlRED,"Usage Error:") );
    fprintf( stderr, " Environment variable possibly not correct. Translations might fail - LANG=%s\n", oyjlTermColor(oyjlBOLD,lang) );
  }
  if(lang)
    loc = lang;
  if(loc)
  {
    const char * my_domain = MY_DOMAIN;
# include "abc2html.i18n.h"
    int size = sizeof(abc2html_i18n_oiJS);
    oyjl_val static_catalog = (oyjl_val) oyjlStringAppendN( NULL, (const char*) abc2html_i18n_oiJS, size, malloc );
    if(my_domain && strcmp(my_domain,"oyjl") == 0)
      my_domain = NULL;
    trc = trc_ = oyjlTranslation_New( loc, my_domain, &static_catalog,0,0,0,0 );
  }
  oyjlInitLanguageDebug( "abc2html", NULL, NULL, use_gettext, NULL, NULL, &trc_, NULL );
  if(MY_DOMAIN && strcmp(MY_DOMAIN,"oyjl") == 0)
    trc = oyjlTranslation_Get( MY_DOMAIN );

  myMain(argc, (const char **)argv);

  oyjlTranslation_Release( &trc_ );
  oyjlLibRelease();

#ifdef __ANDROID__
  free( argv );
#endif

  return 0;
}
#endif

/*--------------8<------------------- xdg-user-dir-lookup.c ---------*/
/*
  This file is not licenced under the GPL like the rest of the code.
  Its is under the MIT license, to encourage reuse by cut-and-paste.

  Copyright (c) 2007 Red Hat, Inc.

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions: 

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software. 

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * xdg_user_dir_lookup_with_fallback:
 * @type: a string specifying the type of directory
 * @fallback: value to use if the directory isn't specified by the user
 * @returns: a newly allocated absolute pathname
 *
 * Looks up a XDG user directory of the specified type.
 * Example of types are "DESKTOP" and "DOWNLOAD".
 *
 * In case the user hasn't specified any directory for the specified
 * type the value returned is @fallback.
 *
 * The return value is newly allocated and must be freed with
 * free(). The return value is never NULL if @fallback != NULL, unless
 * out of memory.
 **/
static char *
xdg_user_dir_lookup_with_fallback (const char *type, const char *fallback)
{
  FILE *file;
  char *home_dir, *config_home, *config_file;
  char buffer[512];
  char *user_dir;
  char *p, *d;
  int len;
  int relative;
  
  home_dir = getenv ("HOME");

  if (home_dir == NULL)
    goto error;

  config_home = getenv ("XDG_CONFIG_HOME");
  if (config_home == NULL || config_home[0] == 0)
    {
      config_file = (char*) malloc (strlen (home_dir) + strlen ("/.config/user-dirs.dirs") + 1);
      if (config_file == NULL)
        goto error;

      strcpy (config_file, home_dir);
      strcat (config_file, "/.config/user-dirs.dirs");
    }
  else
    {
      config_file = (char*) malloc (strlen (config_home) + strlen ("/user-dirs.dirs") + 1);
      if (config_file == NULL)
        goto error;

      strcpy (config_file, config_home);
      strcat (config_file, "/user-dirs.dirs");
    }

  file = fopen (config_file, "r");
  free (config_file);
  if (file == NULL)
    goto error;

  user_dir = NULL;
  while (fgets (buffer, sizeof (buffer), file))
    {
      /* Remove newline at end */
      len = strlen (buffer);
      if (len > 0 && buffer[len-1] == '\n')
	buffer[len-1] = 0;
      
      p = buffer;
      while (*p == ' ' || *p == '\t')
	p++;
      
      if (strncmp (p, "XDG_", 4) != 0)
	continue;
      p += 4;
      if (strncasecmp (p, type, strlen (type)) != 0)
	continue;
      p += strlen (type);
      if (strncmp (p, "_DIR", 4) != 0)
	continue;
      p += 4;

      while (*p == ' ' || *p == '\t')
	p++;

      if (*p != '=')
	continue;
      p++;
      
      while (*p == ' ' || *p == '\t')
	p++;

      if (*p != '"')
	continue;
      p++;
      
      relative = 0;
      if (strncmp (p, "$HOME/", 6) == 0)
	{
	  p += 6;
	  relative = 1;
	}
      else if (*p != '/')
	continue;
      
      if (relative)
	{
	  user_dir = (char*) malloc (strlen (home_dir) + 1 + strlen (p) + 1);
          if (user_dir == NULL)
            goto error2;

	  strcpy (user_dir, home_dir);
	  strcat (user_dir, "/");
	}
      else
	{
	  user_dir = (char*) malloc (strlen (p) + 1);
          if (user_dir == NULL)
            goto error2;

	  *user_dir = 0;
	}
      
      d = user_dir + strlen (user_dir);
      while (*p && *p != '"')
	{
	  if ((*p == '\\') && (*(p+1) != 0))
	    p++;
	  *d++ = *p++;
	}
      *d = 0;
    }
error2:
  fclose (file);

  if (user_dir)
    return user_dir;

 error:
  if (fallback)
    return strdup (fallback);
  return NULL;
}

/**
 * xdg_user_dir_lookup:
 * @type: a string specifying the type of directory
 * @returns: a newly allocated absolute pathname
 *
 * Looks up a XDG user directory of the specified type.
 * Example of types are "DESKTOP" and "DOWNLOAD".
 *
 * The return value is always != NULL (unless out of memory),
 * and if a directory
 * for the type is not specified by the user the default
 * is the home directory. Except for DESKTOP which defaults
 * to ~/Desktop.
 *
 * The return value is newly allocated and must be freed with
 * free().
 **/
static char *
xdg_user_dir_lookup (const char *type)
{
  char *dir, *home_dir, *user_dir;
	  
  dir = xdg_user_dir_lookup_with_fallback (type, NULL);
  if (dir != NULL)
    return dir;
  
  home_dir = getenv ("HOME");
  
  if (home_dir == NULL)
    return strdup ("/tmp");
  
  /* Special case desktop for historical compatibility */
  if (strcmp (type, "DESKTOP") == 0)
    {
      user_dir = (char*) malloc (strlen (home_dir) + strlen ("/Desktop") + 1);
      if (user_dir == NULL)
        return NULL;

      strcpy (user_dir, home_dir);
      strcat (user_dir, "/Desktop");
      return user_dir;
    }
  
  return strdup (home_dir);
}

#ifdef STANDALONE_XDG_USER_DIR_LOOKUP
int
main (int argc, char *argv[])
{
  if (argc != 2)
    {
      fprintf (stderr, "Usage %s <dir-type>\n", argv[0]);
      exit (1);
    }
  
  printf ("%s\n", xdg_user_dir_lookup (argv[1]));
  return 0;
}
#endif
/*--------------8<------------------- xdg-user-dir-lookup.c ---------*/

