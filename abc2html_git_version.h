/** @file  abc2html_git_version.h
 *  @brief automatic generated variables
 */
#ifndef ABC2HTML_GIT_VERSION_H
#define ABC2HTML_GIT_VERSION_H

#define ABC2HTML_GIT_VERSION               "35-g438ac50-2025-02-16"  /**< git describe --always HEAD  + date */
#define ABC2HTML_GIT_DATE                  "2025-02-16"
#define ABC2HTML_GIT_DATE_LONG             "16 February 2025" 

#endif /*ABC2HTML_GIT_VERSION_H*/

