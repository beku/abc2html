/** @file test.c
 *
 *  abc2html testing
 *
 *  Copyright (C) 2024  Kai-Uwe Behrmann
 *
 *  @brief    abc2html Tests
 *  @author   Kai-Uwe Behrmann <ku.b@gmx.de>
 *  @par License:\n
 *  new BSD <http://www.opensource.org/licenses/BSD-3-Clause>
 *  @since    2024/04/20
 */

#define OYJL_TEST_NAME "test"

#include "abc2html_version.h"

#define TESTS_RUN \
  TEST_RUN( testToolAbc2html, "Tool abc2html", 1 ); \
  TEST_RUN( testCoreAbc2html, "abc2html core functions", 1 );

void oyjlLibRelease();
#define OYJL_TEST_MAIN_SETUP  printf("\n    %s Program\n", oyjlTermColor(oyjlBOLD, "abc2html Test"));

#include "oyjl.h"
#include "oyjl_test_main.h"

/* --- actual tests --- */

typedef struct {
  const char * command; /* command line to exercise and read back in with oyjlReadCommandF() */
  size_t       result;  /* size of the expected result of command */
  const char * string;  /* content of the expected result of command */
  const char * print;   /* print instead of showing plain command line */
  int          result_max; /* maximum size of the expected result of command. changes interpretation of "result" member to a minimum value; ignore when zero */
} oyjl_command_test_s;

oyjlTESTRESULT_e   testTool          ( const char        * prog,
                                       size_t              help_size,
                                       oyjl_command_test_s*commands,
                                       int                 count,
                                       oyjlTESTRESULT_e    result,
                                       oyjlTESTRESULT_e    fail )
{
  size_t len;
  int size;
  char info[48];
  const char * plain;

  fprintf( zout, "testing: %s  %d tests\n", oyjlTermColorF(oyjlBOLD, prog), count );

  char * command = NULL, *t;
  oyjlStringAdd( &command, 0,0, "./%s", prog );
  if(command)
  {
    if(verbose)
      fprintf( stderr, "detecting: %s\n", oyjlTermColor( oyjlBOLD, command ) );
    size = oyjlIsFile( command, "r", OYJL_NO_CHECK, info, 48 );
    if(!size || verbose)
    {
      fprintf(stderr, "%sread: %s %s %d\n", size == 0?"Could not ":"", oyjlTermColor(oyjlBOLD,prog), info, size);
    }
    free(command);
    command = NULL;
  }

  t = oyjlReadCommandF( &size, "r", malloc, "%s%s --help", strstr(prog,  "LANG=")?"":"LANG=C ./", prog );
  plain = oyjlTermColorToPlain(t, 0);
  len = t ? strlen(plain) : 0;
  if(len == help_size)
  { PRINT_SUB_INT( oyjlTESTRESULT_SUCCESS, len,
    "%s --help", prog );
  } else
  { PRINT_SUB_INT( fail, len,
    "%s --help", prog );
  }
  OYJL_TEST_WRITE_RESULT( t, size, prog, "txt" )
  if(verbose && len)
    fprintf( zout, "%s\n", t );
  if(t) {free(t);}

  int i;
  for(i = 0; i < count; ++i)
  {
    char * cmd = NULL;
    oyjl_command_test_s * command = &commands[i];
    const char * args = command->command;
    size_t result_size = command->result;
    int result_size_max = command->result_max;
    char * result_string = oyjlStringCopy( command->string, 0 );
    const char * print = command->print;

    oyjlStringAdd( &cmd, 0,0, "%s %s", prog, args );

    if(verbose)
      fprintf( stderr, "cmd: %s : %ld\n", cmd, result_size );

    t = oyjlReadCommandF( &size, "r", malloc, "LANG=C PATH=.:$PATH %s", cmd );
    plain = oyjlTermColorToPlain(t, 0);
    len = t ? strlen(plain) : 0;
    if(len && t[len-1] == '\n' && result_string && result_string[0] && result_string[strlen(result_string)-1] != '\n' && strcmp(result_string,"1+") != 0)
      oyjlStringAdd( &result_string, 0,0, "\n" );
    if( ((len == result_size || result_size == 0) &&
         (!result_string ||
          (result_string && t && strcmp(t, result_string) == 0))) ||
        (result_string && strcmp(result_string,"1+") == 0 && len > 0) ||
        (result_size && result_size_max && result_size < len && len < (size_t)result_size_max) )
    {
      if(result_string && len && result_string[len-1] == '\n') result_string[len-1] = '\000';
      PRINT_SUB_INT( oyjlTESTRESULT_SUCCESS, len,
        "%s%s%s%s%s", oyjlTermColor(oyjlITALIC,print?print:cmd), result_string&&t?" = ":"", result_string&&t?"\"":"", result_string&&t?result_string:"", result_string&&t?"\"":"" );
    } else
    { PRINT_SUB( fail,
      "%s  %lu(%lu) %s", print?print:cmd, len,result_size, result_string&&t?result_string:"" );
    }
    OYJL_TEST_WRITE_RESULT( t, size, prog, "txt" )
    if(verbose && len)
      fprintf( zout, "%s\n", t );
    if(t) {free(t);}

    if(cmd) {free(cmd); cmd = NULL;}
    if(result_string) {free(result_string); result_string = NULL;}
  }

  return result;
}

int abc2htmlGetFileSize(const char *);
char * abc2htmlReplaceFileNameEnding( const char * fn, const char * suffix );

oyjlTESTRESULT_e testTitleOutput     ( const char        * end,
                                       const char        * input,
                                       const char        * title,
                                       int                 index,
                                       int                 min_size,
                                       oyjlTESTRESULT_e    result )
{
  char * t = abc2htmlReplaceFileNameEnding( title, end ), * cmd = NULL;
  int size = 0;
  oyjlStringAdd( &cmd, 0,0, "LANG=C ./abc2html -f=%s -i=%s --select=%d -o==", end, input, index );
  system( cmd );
  size = abc2htmlGetFileSize(t);
  if(size > min_size)
  { PRINT_SUB_INT( oyjlTESTRESULT_SUCCESS, size,
    "abc2html(-f=%s -o==) => \"%s\"", end, oyjlTermColor(oyjlBOLD,t) );
  } else
  { PRINT_SUB_INT( oyjlTESTRESULT_FAIL, size,
    "abc2html(-f=%s -o==) => \"%s\"", end, t );
  }
  if(t) {free(t); t = NULL;}
  if(cmd) {free(cmd); cmd = NULL;}
  return result;
}

oyjlTESTRESULT_e testToolAbc2html ()
{
  oyjlTESTRESULT_e result = oyjlTESTRESULT_UNKNOWN;

  fprintf(stdout, "\n" );
  int help_size =
#ifdef HAVE_MUSICABC
    5492
#else
    5514
#endif
    ;

  oyjl_command_test_s commands[] = {
    { "-l -i=../extras/test.abc",           200,  NULL,       NULL, 0 },
    { "-f=abc -i=../extras/test.abc",      2723,  NULL,       NULL, 0 },
    { "-f=html -i=../extras/test.abc",  2769317,  NULL,       NULL, 0 }
  };
  int count = 3;
  result = testTool( "abc2html", help_size, commands, count, result, oyjlTESTRESULT_FAIL );

#define ABC2SVG "abcm2ps"
  if(oyjlHasApplication(ABC2SVG))
  {
    oyjl_command_test_s commands2[] = {
      { "-f=svg -i=../extras/test.abc -F=select=[3,1]", 15000, NULL, NULL, 22000 },
      { "-f=svgs -i=../extras/test.abc", 80000,   NULL,       NULL, 300000 }
    };
    count = 2;
    result = testTool( "abc2html", help_size, commands2, count, result, oyjlTESTRESULT_FAIL );
  }

  oyjl_command_test_s commands3[] = {
    { "--list-paths -i=~/Musik",              0,  "1+",       NULL, 0 },
    { "--list-files -i=~/Musik",              0,  "1+",       NULL, 0 },
    { "--list-files -i=~/Downloads",          0,  "1+",       NULL, 0 },
    { "--list-files",                         0,  "1+",       NULL, 0 }
  };
  count = 4;
  result = testTool( "abc2html", help_size, commands3, count, result, oyjlTESTRESULT_XFAIL );

#ifdef HAVE_MUSICABC
  int size = 0, len_svg, len_abc, select = 3;;
  char * t = oyjlReadCommandF( &size, "r", malloc, "LANG=C ./abc2html -f=svg -i=../extras/test.abc --select=%d", select ), *t2, *t3;
  len_svg = strlen(t);
  oyjlWriteFile( "test.svg", t, len_svg );
  t2 = oyjlReadCommandF( &size, "r", malloc, "LANG=C ./abc2html -f=abc -i=test.svg" );
  len_abc = size;
  t3 = oyjlReadCommandF( &size, "r", malloc, "LANG=C ./abc2html -f=abc -i=../extras/test.abc --select=%d", select );
  if(size && len_abc && (size == len_abc || len_abc-1 == size))
  { PRINT_SUB_INT( oyjlTESTRESULT_SUCCESS, size,
    "abc2html roundtrip abc(%d bytes) -> svg(%d bytes) -> abc(%d bytes)", len_abc, len_svg, size );
  } else
  { PRINT_SUB_INT( oyjlTESTRESULT_FAIL, size,
    "abc2html roundtrip abc(%d bytes) -> svg(%d bytes) -> abc(%d bytes)", len_abc, len_svg, size );
    if(len_abc != size)
    {
      fputs( t2, zout );
      fputs( t3, zout );
    }
  }
  OYJL_TEST_WRITE_RESULT( t, size, "svgToAbc", "txt" )
  if(verbose && size)
    fprintf( zout, "%s\n", t );
  if(t) {free(t); t = NULL;}

  size = 0;
  system( "LANG=C ./abc2html -f=pdf -i=../extras/test.abc --select=3 -o=test.pdf" );
  len_svg = abc2htmlGetFileSize("test.pdf");
  if(t) {free(t); t = NULL;}
  if(t2) {free(t2); t2 = NULL;}
  if(t3) {free(t3); t3 = NULL;}
  size = 0;
  t = oyjlReadCommandF( &size, "r", malloc, "LANG=C ./abc2html -f=abc -i=test.pdf" );
  len_abc = size;
  if(t) {free(t); t = NULL;}
  size = 0;
  t = oyjlReadCommandF( &size, "r", malloc, "LANG=C ./abc2html -f=abc -i=../extras/test.abc --select=3" );
  if(size && len_abc && size == len_abc)
  { PRINT_SUB_INT( oyjlTESTRESULT_SUCCESS, size,
    "abc2html roundtrip abc(%d bytes) -> pdf(%d bytes) -> abc(%d bytes)", len_abc, len_svg, size );
  } else
  { PRINT_SUB_INT( oyjlTESTRESULT_FAIL, size,
    "abc2html roundtrip abc(%d bytes) -> pdf(%d bytes) -> abc(%d bytes)", len_abc, len_svg, size );
  }
  OYJL_TEST_WRITE_RESULT( t, size, "pdfToAbc", "txt" )
  if(verbose && size)
    fprintf( zout, "%s\n", t );
  if(t) {free(t); t = NULL;}

  const char * titles[] = {
    "Hanter Dro De Languidig",
    "Danse de l'ours",
    "Nun will der Lenz uns grüßen",
    "Blind Mary",
    "Danse de l'ours"
  }; 
  int i, j, min_sizes[] =  {10000,20000,50000};
  const char * formats[] = {"svg","pdf","png"};

  for(i = 0; i < 3; ++i)
  {
    const char * format = formats[i];
    int min_size = min_sizes[i];
    for(j = 0; j < 5; ++j)
      result = testTitleOutput( format, "../extras/test.abc", titles[j], j, min_size, result );
  }

  const char * abc_file = ABC2HTML_DATADIR "/doc/abcm2ps/examples/accordion.abc";
  if(abc2htmlGetFileSize( abc_file ))
  {
    fprintf(zout, "Found: %s\n", abc_file );
    titles[0] = "Andro à Patrice Corbel";
    titles[1] = "Jeune fille de quinze ans (50-51-52)";
    for(i = 0; i < 3; ++i)
    {
      const char * format = formats[i];
      int min_size = min_sizes[i];
      for(j = 0; j < 2; ++j)
        result = testTitleOutput( format, abc_file, titles[j], j, min_size, result );
    }
  }
  else
    fprintf(zout, "Not found: %s\n", abc_file );
#endif

  return result;
}

#define NO_MAIN
#include "abc2html.c"
#undef NO_MAIN

oyjlTESTRESULT_e testCoreAbc2html ()
{
  oyjlTESTRESULT_e result = oyjlTESTRESULT_UNKNOWN;

  fprintf(stdout, "\n" );


  const char * test_text = "Hello Worl";
  int size = strlen( test_text );
  char * data = abc2htmlBas64Encode( (const unsigned char *) test_text, size, verbose );
  int len = strlen(data);
  if(data && len != size && strcmp(data,"SGVsbG8gV29ybA==") == 0)
  { PRINT_SUB_INT( oyjlTESTRESULT_SUCCESS, len,
    "abc2htmlBas64Encode(%s)   = %s", oyjlTermColor(oyjlBOLD,test_text), oyjlTermColorF(oyjlITALIC,"%s", data) );
  } else
  { PRINT_SUB_INT( oyjlTESTRESULT_FAIL, len,
    "abc2htmlBas64Encode(%s)   = %s", oyjlTermColor(oyjlBOLD,test_text), oyjlTermColorF(oyjlITALIC,"%s", data) );
  }
  if(data) {free(data);}

  test_text = "Hello World";
  size = strlen( test_text );
  data = abc2htmlBas64Encode( (const unsigned char *) test_text, size, verbose );
  len = strlen(data);
  if(data && len != size && strcmp(data,"SGVsbG8gV29ybGQ=") == 0)
  { PRINT_SUB_INT( oyjlTESTRESULT_SUCCESS, len,
    "abc2htmlBas64Encode(%s)  = %s", oyjlTermColor(oyjlBOLD,test_text), oyjlTermColorF(oyjlITALIC,"%s", data) );
  } else
  { PRINT_SUB_INT( oyjlTESTRESULT_FAIL, len,
    "abc2htmlBas64Encode(%s)  = %s", oyjlTermColor(oyjlBOLD,test_text), oyjlTermColorF(oyjlITALIC,"%s", data) );
  }
  if(data) {free(data);}

  test_text = "Hello World!";
  size = strlen( test_text );
  data = abc2htmlBas64Encode( (const unsigned char *) test_text, size, verbose );
  len = strlen(data);
  if(data && len != size && strcmp(data,"SGVsbG8gV29ybGQh") == 0)
  { PRINT_SUB_INT( oyjlTESTRESULT_SUCCESS, len,
    "abc2htmlBas64Encode(%s) = %s", oyjlTermColor(oyjlBOLD,test_text), oyjlTermColorF(oyjlITALIC,"%s", data) );
  } else
  { PRINT_SUB_INT( oyjlTESTRESULT_FAIL, len,
    "abc2htmlBas64Encode(%s) = %s", oyjlTermColor(oyjlBOLD,test_text), oyjlTermColorF(oyjlITALIC,"%s", data) );
  }
  if(data) {free(data);}

  return result;
}



/* --- end actual tests --- */


