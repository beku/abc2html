abc2html README
===============
[![Pipeline](https://gitlab.com/beku/abc2html/badges/master/pipeline.svg)](https://gitlab.com/beku/abc2html/-/pipelines)
[![Documentation](extras/images/tool-documented.svg)](docs/md/abc2html.md)
[![License: AGPL v3](extras/images/License-AGPL_v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

![Logo](extras/images/logo.svg)

The tool packs abc music notation file(s) into a html page for notes reading and listening. Informations are provided for listing titles, files and paths. Filtering of ABC files is possible.


Features
--------
* Convert to static HTML with embedded **MusicABC** and **JavaScript** for easy single file sharing in a web browser
* Convert back to **MusicABC** from above HTML for editing
* Convert to SVG with external **abcm2ps** tool
* Convert to PDF with external **inkscape** and **pdftk**
* List of ABC titles, files and search paths

Documentation
-------------
[abc2html Tool Syntax](docs/md/abc2html.md) [de](docs/md/abc2htmlde.md)


Dependencies
------------
* [Oyjl](https://gitlab.com/beku/oyjl) - Oyjl Shared Basics for CLI parsing, documentation, translation and additional GUIs

### Optional
* [libMusicAbc](https://gitlab.com/beku/libmusicabc) - libMusicAbc converts music tunes from the ABC music notation to SVG
* [abcm2ps](http://moinejf.free.fr/) - for SVG creation without libMusicAbc
* diff - for difference print of duplicate titles
* [inkscape](https://inkscape.org/) - for SVG -> PDF conversion
* [pdftk](https://www.pdflabs.com/tools/pdftk-server/) - for PDF assembly and ToC


Building
--------
Supported are cmake builds.

    $ mkdir build && cd build
    $ cmake ..
    $ make
    $ make install

### Build Flags
... are typical cmake flags like CMAKE\_C\_FLAGS to tune compilation.

* CMAKE\_INSTALL\_PREFIX to install into paths and so on. Use on the command 
  line through -DCMAKE\_INSTALL\_PREFIX=/my/path .

