# abc2html v0.5.1 {#abc2html}
<a name="toc"></a>
[NAME](#name) [SYNOPSIS](#synopsis) [DESCRIPTION](#description) [OPTIONS](#options) [GENERAL OPTIONS](#general_options) [EXAMPLES](#examples) [SYNTAX](#syntax) [SEE ALSO](#seealso) [AUTHOR](#author) [COPYRIGHT](#copyright) 

<strong>"abc2html"</strong> *1* <em>"16 February 2025"</em> "User Commands"

<h2>NAME <a href="#toc" name="name">&uarr;</a></h2>

abc2html v0.5.1 - ABC Conversion

<h2>SYNOPSIS <a href="#toc" name="synopsis">&uarr;</a></h2>

<strong>abc2html</strong> <a href="#format"><strong>-f</strong>=<em>FORMAT</em></a> [<strong>-i</strong>=<em>FILENAME</em>] [<strong>-F</strong>=<em>INSTRUCTION</em>] [<strong>--select</strong>=<em>NUMBER</em>] [<strong>-r</strong><em>[=INSTRUCTION]</em>] [<strong>-o</strong>=<em>FILENAME</em>] [<strong>-t</strong>=<em>NAME</em>] [<strong>-s</strong>=<em>FILENAME</em>] [<strong>-a</strong>=<em>FILENAME</em>] [<strong>--ifnot-grid2</strong>=<em>FILENAME</em>] [<strong>--if-grid2</strong>=<em>FILENAME</em>] [<strong>--abc-priority</strong>=<em>FILENAME</em>] [<strong>-c</strong>=<em>FILENAME</em>] [<strong>-b</strong>] [<strong>-v</strong>]
<br />
<strong>abc2html</strong> <a href="#list"><strong>-l</strong><em>[=HINTS]</em></a> | <strong>-P</strong> | <strong>--list-files</strong> [<strong>-i</strong>=<em>FILENAME</em>] [<strong>-F</strong>=<em>INSTRUCTION</em>] [<strong>-v</strong>]
<br />
<strong>abc2html</strong> <a href="#export"><strong>-X</strong>=<em>json|json+command|man|markdown</em></a> | <strong>-h</strong><em>[=synopsis|...]</em> | <strong>-V</strong> | <strong>-R</strong>=<em>gui|cli|web|...</em> [<strong>-v</strong>]

<h2>DESCRIPTION <a href="#toc" name="description">&uarr;</a></h2>

The tool packs abc music notation file(s) into a html page for notes reading and listening. Informations are provided for listing titles, files and paths. Filtering of ABC files is possible.

<h2>OPTIONS <a href="#toc" name="options">&uarr;</a></h2>

<h3 id="format">Conversion</h3>

&nbsp;&nbsp; <a href="#synopsis"><strong>abc2html</strong></a> <strong>-f</strong>=<em>FORMAT</em> [<strong>-i</strong>=<em>FILENAME</em>] [<strong>-F</strong>=<em>INSTRUCTION</em>] [<strong>--select</strong>=<em>NUMBER</em>] [<strong>-r</strong><em>[=INSTRUCTION]</em>] [<strong>-o</strong>=<em>FILENAME</em>] [<strong>-t</strong>=<em>NAME</em>] [<strong>-s</strong>=<em>FILENAME</em>] [<strong>-a</strong>=<em>FILENAME</em>] [<strong>--ifnot-grid2</strong>=<em>FILENAME</em>] [<strong>--if-grid2</strong>=<em>FILENAME</em>] [<strong>--abc-priority</strong>=<em>FILENAME</em>] [<strong>-c</strong>=<em>FILENAME</em>] [<strong>-b</strong>] [<strong>-v</strong>]

&nbsp;&nbsp;Convert Music ABC to various formats. Extract ABC back, when embedded as text form in HTML. ABC selection is done by lists.

<table style='width:100%'>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-f</strong>|<strong>--format</strong>=<em>FORMAT</em></td> <td>Write to specified data format.
  <table>
   <tr><td style='padding-left:0.5em'><strong>-f</strong> ABC</td><td># ABC : Strip HTML processing lines - The "-r" option allows to fine tune by extra filters.</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-f</strong> HTML</td><td># HTML : Generate a self contained HTML Page - See as well the -t, -s, -a and -c Options</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-f</strong> SVG</td><td># SVG : Generate a SVG Document - Use libMusicAbc</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-f</strong> PNG</td><td># PNG : Generate a PNG Document - Requires rsvg-convert</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-f</strong> PDF</td><td># PDF : Generate a PDF Document - Requires rsvg-convert and pdftk</td></tr>
  </table>
  </td>
 </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-F</strong>|<strong>--filter</strong>=<em>INSTRUCTION</em></td> <td>Select by Filter "header" or "select"<br />Suboption header with comma ',' separated list of fields with possibly '=' assigned filter.
  <table>
   <tr><td style='padding-left:0.5em'><strong>-F</strong> header=C=trad</td><td># header=C=trad : Select C - Select only traditional composers</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-F</strong> header=R=Jig,C=Trad</td><td># header=R=Jig,C=Trad : Select R+C - Select only tradional jigs.</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-F</strong> select=[ 1, 2, 3, 5 ]</td><td># select=[ 1, 2, 3, 5 ] : Select Tunes - Select by Position or Title.</td></tr>
  </table>
  </td>
 </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>--select</strong>=<em>NUMBER</em></td> <td>Select Single Tune (NUMBER:0 [≥0 ≤60 Δ1])</td> </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-r</strong>|<strong>--remove</strong><em>[=INSTRUCTION]</em></td> <td>Remove Instructions<br />Suboption header with comma ',' separated list of filters.
  <table>
   <tr><td style='padding-left:0.5em'><strong>-r</strong> extra_abc</td><td># extra_abc : Remove %% inside - Clean inside ABC. Useful for plain reading.</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-r</strong> -extra_abc</td><td># -extra_abc : Skip %% - Keep additional ABC code at start and end of ABC. Shows ABC injections from HTML page.</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-r</strong> -html</td><td># -html : Skip HTML - Keep HTML in place.</td></tr>
  </table>
  </td>
 </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-i</strong>|<strong>--input</strong>=<em>FILENAME</em></td> <td>File or Stream<br />A file name or a input stream like "stdin". </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-o</strong>|<strong>--output</strong>=<em>FILENAME</em></td> <td>File or Stream<br />A file name or a output stream like "stdout". The equal sign '=' (-o==) will with HTML be used for reusing the input file name with the appropriate ending. All other types will be renamed to the T: ABC title with '=' (-o==). </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-t</strong>|<strong>--title</strong>=<em>NAME</em></td> <td>Page Title<br />By default it will be taken from output or input. </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-s</strong>|<strong>--style</strong>=<em>FILENAME</em></td> <td>Use HTML CSS + ABCheader File<br />Select a file to use as css_layout2 + abc_intro. </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-a</strong>|<strong>--abc-intro</strong>=<em>FILENAME</em></td> <td>Use MusicABC Header File<br />Select a file to use as abc_intro. </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>--ifnot-grid2</strong>=<em>FILENAME</em></td> <td>Use MusicABC Header File<br />Select a file to use as ifnot_grid2. </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>--if-grid2</strong>=<em>FILENAME</em></td> <td>Use MusicABC Header File<br />Select a file to use as if_grid2. </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>--abc-priority</strong>=<em>FILENAME</em></td> <td>Use MusicABC Header File<br />Select a file to use with slight priority. </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-c</strong>|<strong>--css-layout2</strong>=<em>FILENAME</em></td> <td>Use HTML CSS File<br />Select a file to use as css_layout2. </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-b</strong>|<strong>--borderless</strong></td> <td>Skip page borders.<br />Applies to formats -f=svg and -f=pdf only.</td> </tr>
</table>

<h3 id="list">Information</h3>

&nbsp;&nbsp; <a href="#synopsis"><strong>abc2html</strong></a> <strong>-l</strong><em>[=HINTS]</em> | <strong>-P</strong> | <strong>--list-files</strong> [<strong>-i</strong>=<em>FILENAME</em>] [<strong>-F</strong>=<em>INSTRUCTION</em>] [<strong>-v</strong>]

&nbsp;&nbsp;Create Lists of ABC titles and show file locations and search paths.

<table style='width:100%'>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-l</strong>|<strong>--list</strong><em>[=HINTS]</em></td> <td>List<br />Accepts display hints of header lines. Example: -l=header=T,R,C . Add ,1 for one tune per line display. E.g -l=header=K,T,1 .  </td>
 </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-i</strong>|<strong>--input</strong>=<em>FILENAME</em></td> <td>File or Stream<br />A file name or a input stream like "stdin". </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-P</strong>|<strong>--list-paths</strong></td> <td>List Music ABC Search Paths</td> </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>--list-files</strong></td> <td>List Music ABC Files</td> </tr>
</table>


<h2>GENERAL OPTIONS <a href="#toc" name="general_options">&uarr;</a></h2>

<h3 id="export">General options</h3>

&nbsp;&nbsp; <a href="#synopsis"><strong>abc2html</strong></a> <strong>-X</strong>=<em>json|json+command|man|markdown</em> | <strong>-h</strong><em>[=synopsis|...]</em> | <strong>-V</strong> | <strong>-R</strong>=<em>gui|cli|web|...</em> [<strong>-v</strong>]

<table style='width:100%'>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-X</strong>|<strong>--export</strong>=<em>json|json+command|man|markdown</em></td> <td>Export formated text<br />Get UI converted into text formats
  <table>
   <tr><td style='padding-left:0.5em'><strong>-X</strong> man</td><td># Man : Unix Man page - Get a unix man page</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-X</strong> markdown</td><td># Markdown : Formated text - Get formated text</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-X</strong> json</td><td># Json : GUI - Get a Oyjl Json UI declaration</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-X</strong> json+command</td><td># Json + Command : GUI + Command - Get Oyjl Json UI declaration incuding command</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-X</strong> export</td><td># Export : All available data - Get UI data for developers. The format can be converted by the oyjl-args tool.</td></tr>
  </table>
  </td>
 </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-h</strong>|<strong>--help</strong><em>[=synopsis|...]</em></td> <td>Print help text<br />Show usage information and hints for the tool.
  <table>
   <tr><td style='padding-left:0.5em'><strong>-h</strong> -</td><td># Full Help : Print help for all groups</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-h</strong> synopsis</td><td># Synopsis : List groups - Show all groups including syntax</td></tr>
  </table>
  </td>
 </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-V</strong>|<strong>--version</strong></td> <td>Version</td> </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-R</strong>|<strong>--render</strong>=<em>gui|cli|web|...</em></td> <td>Render
  <table>
   <tr><td style='padding-left:0.5em'><strong>-R</strong> gui</td><td># Gui : Show UI - Display a interactive graphical User Interface.</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-R</strong> cli</td><td># Cli : Show UI - Print on Command Line Interface.</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-R</strong> web</td><td># Web : Start Web Server - Start a local Web Service to connect a Webbrowser with. Use the -R=web;help sub option to see more information.</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-R</strong> -</td>
  </table>
  </td>
 </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-v</strong>|<strong>--verbose</strong></td> <td>Verbose</td> </tr>
</table>


<h2>EXAMPLES <a href="#toc" name="examples">&uarr;</a></h2>

#### List Music ABC Titles
&nbsp;&nbsp;abc2html -l -i=my.abc
#### List Music ABC Titles and show T and R header fields
&nbsp;&nbsp;abc2html -l=header=T,R -i=my.abc
#### List Music ABC Files
&nbsp;&nbsp;abc2html --list-files -i=~/Downloads
#### List Music ABC Search Paths
&nbsp;&nbsp;abc2html -P -i=~
#### Convert Music ABC embedded in HTML to ABC
&nbsp;&nbsp;abc2html -f=abc -i=my.abc.html
#### Convert Music ABC to HTML
&nbsp;&nbsp;abc2html -f=html -i=my.abc -o=my.abc.html
#### Convert Music ABC to SVG
&nbsp;&nbsp;abc2html -f=svg -i=my.abc -F=select=[11] -o=my.abc.svg
#### Convert Music ABC to PDF
&nbsp;&nbsp;abc2html -f=pdf -i=my.abc -F=select=[11,"Title",3] -o=my.abc.pdf

<h2>SYNTAX <a href="#toc" name="syntax">&uarr;</a></h2>

#### F: file-name.[png|svg|jpg]
&nbsp;&nbsp;The three image formats are supported for the --format=HTML output.
  <br />
&nbsp;&nbsp;The F headline is repeatable.

<h2>SEE ALSO <a href="#toc" name="seealso">&uarr;</a></h2>

 ABC Music Format

&nbsp;&nbsp;<a href="https://abcnotation.com">https://abcnotation.com</a>
&nbsp;&nbsp;[abcm2ps](abcm2ps.html)<a href="abcm2ps.md">(1)</a>

&nbsp;&nbsp;<a href="http://moinejf.free.fr/">http://moinejf.free.fr/</a>
 ABC_UI

&nbsp;&nbsp;<a href="http://dev.music.free.fr/index.html">http://dev.music.free.fr/index.html</a>
 Musyng Kite Soundfont

&nbsp;&nbsp;<a href="https://gleitz.github.io/midi-js-soundfonts/">https://gleitz.github.io/midi-js-soundfonts/</a>

<h2>AUTHOR <a href="#toc" name="author">&uarr;</a></h2>

Kai-Uwe Behrmann ku.b@gmx.de

<h2>COPYRIGHT <a href="#toc" name="copyright">&uarr;</a></h2>

*Copyright © 2016-2025 Kai-Uwe Behrmann*


<a name="license"></a>
### License
AGPL-3.0 <a href="https://opensource.org/licenses/AGPL-3.0">https://opensource.org/licenses/AGPL-3.0</a>

