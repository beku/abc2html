# abc2html v0.5.1 {#abc2htmlde}
<a name="toc"></a>
[NAME](#name) [ÜBERSICHT](#synopsis) [BESCHREIBUNG](#description) [OPTIONEN](#options) [ALLGEMEINE OPTIONEN](#general_options) [BEISPIELE](#examples) [SYNTAX](#syntax) [SIEHE AUCH](#seealso) [AUTOR](#author) [KOPIERRECHT](#copyright) 

<strong>"abc2html"</strong> *1* <em>"16 February 2025"</em> "User Commands"

<h2>NAME <a href="#toc" name="name">&uarr;</a></h2>

abc2html v0.5.1 - ABC Umwandlung

<h2>ÜBERSICHT <a href="#toc" name="synopsis">&uarr;</a></h2>

<strong>abc2html</strong> <a href="#format"><strong>-f</strong>=<em>FORMAT</em></a> [<strong>-i</strong>=<em>DATEINAME</em>] [<strong>-F</strong>=<em>ANWEISUNG</em>] [<strong>--select</strong>=<em>NUMMER</em>] [<strong>-r</strong><em>[=ANWEISUNG]</em>] [<strong>-o</strong>=<em>DATEINAME</em>] [<strong>-t</strong>=<em>NAME</em>] [<strong>-s</strong>=<em>DATEINAME</em>] [<strong>-a</strong>=<em>DATEINAME</em>] [<strong>--ifnot-grid2</strong>=<em>DATEINAME</em>] [<strong>--if-grid2</strong>=<em>DATEINAME</em>] [<strong>--abc-priority</strong>=<em>DATEINAME</em>] [<strong>-c</strong>=<em>DATEINAME</em>] [<strong>-b</strong>] [<strong>-v</strong>]
<br />
<strong>abc2html</strong> <a href="#list"><strong>-l</strong><em>[=ANZEIGE]</em></a> | <strong>-P</strong> | <strong>--list-files</strong> [<strong>-i</strong>=<em>DATEINAME</em>] [<strong>-F</strong>=<em>ANWEISUNG</em>] [<strong>-v</strong>]
<br />
<strong>abc2html</strong> <a href="#export"><strong>-X</strong>=<em>json|json+command|man|markdown</em></a> | <strong>-h</strong><em>[=synopsis|...]</em> | <strong>-V</strong> | <strong>-R</strong>=<em>gui|cli|web|...</em> [<strong>-v</strong>]

<h2>BESCHREIBUNG <a href="#toc" name="description">&uarr;</a></h2>

Das Werkzeug stellt ABC Musiknotationsdateien in einer HTML-Seite zusammen zum Lesen und Hören. Informationen werden angezeigt als Titel-, Datei-und Pfadlisten. Filtern ist möglich.

<h2>OPTIONEN <a href="#toc" name="options">&uarr;</a></h2>

<h3 id="format">Umwandlung</h3>

&nbsp;&nbsp; <a href="#synopsis"><strong>abc2html</strong></a> <strong>-f</strong>=<em>FORMAT</em> [<strong>-i</strong>=<em>DATEINAME</em>] [<strong>-F</strong>=<em>ANWEISUNG</em>] [<strong>--select</strong>=<em>NUMMER</em>] [<strong>-r</strong><em>[=ANWEISUNG]</em>] [<strong>-o</strong>=<em>DATEINAME</em>] [<strong>-t</strong>=<em>NAME</em>] [<strong>-s</strong>=<em>DATEINAME</em>] [<strong>-a</strong>=<em>DATEINAME</em>] [<strong>--ifnot-grid2</strong>=<em>DATEINAME</em>] [<strong>--if-grid2</strong>=<em>DATEINAME</em>] [<strong>--abc-priority</strong>=<em>DATEINAME</em>] [<strong>-c</strong>=<em>DATEINAME</em>] [<strong>-b</strong>] [<strong>-v</strong>]

&nbsp;&nbsp;Wandle Musik ABC in verschiedene Formate. Lese eingebettete ABC Text aus HTML aus. ABC Auswahl wird als Listen akzeptiert.

<table style='width:100%'>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-f</strong>|<strong>--format</strong>=<em>FORMAT</em></td> <td>Schreibe im Datenformat
  <table>
   <tr><td style='padding-left:0.5em'><strong>-f</strong> ABC</td><td># ABC : Lösche HTML Einfügezeilen - Die "-r" Option erlaubt eine genauere Auswahl durch zusätzliche Filter. </td></tr>
   <tr><td style='padding-left:0.5em'><strong>-f</strong> HTML</td><td># HTML : Erzeuge eine transportable HTML Seite - Siehe auch die -t, -s, -a und -c Optionen</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-f</strong> SVG</td><td># SVG : Erzeuge ein SVG Dokument - Benutze libMusicAbc</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-f</strong> PNG</td><td># PNG : Erzeuge ein PNG Dokument - Benötigt rsvg-convert</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-f</strong> PDF</td><td># PDF : Erzeuge ein PDF Dokument - Benötigt rsvg-convert und pdftk</td></tr>
  </table>
  </td>
 </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-F</strong>|<strong>--filter</strong>=<em>ANWEISUNG</em></td> <td>Wähle mit Filter "header" oder "select"<br />Unteroption "header" mit kommagetrennter ',' Feldliste und möglichen '=' zugewiesenen Filtern.
  <table>
   <tr><td style='padding-left:0.5em'><strong>-F</strong> header=C=trad</td><td># header=C=trad : Wähle C - Wähle einzig traditionelle Überlieferung als Komponist</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-F</strong> header=R=Jig,C=Trad</td><td># header=R=Jig,C=Trad : Wähle R+C - Wähle nur traditionelle Jigs.</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-F</strong> select=[ 1, 2, 3, 5 ]</td><td># select=[ 1, 2, 3, 5 ] : Wähle Lieder - Wähle nach Position oder Titel.</td></tr>
  </table>
  </td>
 </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>--select</strong>=<em>NUMMER</em></td> <td>Wähle Einzelnes Stück (NUMMER:0 [≥0 ≤60 Δ1])</td> </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-r</strong>|<strong>--remove</strong><em>[=ANWEISUNG]</em></td> <td>Lösche Anweisungen<br />Unteroption "header" mit kommagetrennter ',' Feldliste.
  <table>
   <tr><td style='padding-left:0.5em'><strong>-r</strong> extra_abc</td><td># extra_abc : Lösche inneres %% - Lösche innerhalb ABC. Nützlich für übersichtliches Lesen.</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-r</strong> -extra_abc</td><td># -extra_abc : Überspringe %% - Lasse zusätzlichen ABC Auszeichnungen am Start und Ende des ABCs. Zeigt ABC Einfügungen von HTML Seite.</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-r</strong> -html</td><td># -html : Überspringe HTML - Lasse HTML stehen.</td></tr>
  </table>
  </td>
 </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-i</strong>|<strong>--input</strong>=<em>DATEINAME</em></td> <td>Datei oder Datenstrom<br />Ein Dateiname oder Eingangsdatenstrom wie "stdin". </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-o</strong>|<strong>--output</strong>=<em>DATEINAME</em></td> <td>Datei oder Datenstrom<br />Ein Dateiname oder Ausgangsdatenstrom wie "stdout". Das Gleichheitszeichen '=' (-o==) wird mit HTML zum Wiederverwenden des Eingangsdateinames mit dann angepasster Endung verwendet werden. Bei allen anderen Formate wird mit '=' (-o==) jeweils in einen Dateinamen mit dem ABC T: Titel des Liedes geschrieben. </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-t</strong>|<strong>--title</strong>=<em>NAME</em></td> <td>Seitenname<br />Vorgabe ist die Aus-oder Eingabe </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-s</strong>|<strong>--style</strong>=<em>DATEINAME</em></td> <td>Benutze HTML CSS + Musik ABC Kopf Datei<br />Wähle eine Datei für css_layout2 + abc_intro. </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-a</strong>|<strong>--abc-intro</strong>=<em>DATEINAME</em></td> <td>Benutze Musik ABC Kopf von Datei<br />Wähle eine Datei für abc_intro. </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>--ifnot-grid2</strong>=<em>DATEINAME</em></td> <td>Benutze Musik ABC Kopf von Datei<br />Wähle eine Datei für abc ifnot_grid2. </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>--if-grid2</strong>=<em>DATEINAME</em></td> <td>Benutze Musik ABC Kopf von Datei<br />Wähle eine Datei für abc if_grid2. </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>--abc-priority</strong>=<em>DATEINAME</em></td> <td>Benutze Musik ABC Kopf von Datei<br />Wähle eine Datei für bevorzugte ABC Kopfeinfügungen. </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-c</strong>|<strong>--css-layout2</strong>=<em>DATEINAME</em></td> <td>Benutze HTML CSS Datei<br />Wähle eine Datei für css_layout2. </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-b</strong>|<strong>--borderless</strong></td> <td>Lasse Seitenränder weg.<br />Wirkt sich nur auf die Formate -f=svg und -f=pdf aus.</td> </tr>
</table>

<h3 id="list">Informationen</h3>

&nbsp;&nbsp; <a href="#synopsis"><strong>abc2html</strong></a> <strong>-l</strong><em>[=ANZEIGE]</em> | <strong>-P</strong> | <strong>--list-files</strong> [<strong>-i</strong>=<em>DATEINAME</em>] [<strong>-F</strong>=<em>ANWEISUNG</em>] [<strong>-v</strong>]

&nbsp;&nbsp;Erzeuge Listen von ABC Titeln und zeige Dateiorte und Suchpfade an.

<table style='width:100%'>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-l</strong>|<strong>--list</strong><em>[=ANZEIGE]</em></td> <td>Liste<br />Wählt Anzeige von Kopfzeilen. z.B.: -l=header=T,R,C . Hänge ,1 an, um auf nur einer Zeile pro Lied anzuzeigen. z.B -l=header=K,T,1 .  </td>
 </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-i</strong>|<strong>--input</strong>=<em>DATEINAME</em></td> <td>Datei oder Datenstrom<br />Ein Dateiname oder Eingangsdatenstrom wie "stdin". </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-P</strong>|<strong>--list-paths</strong></td> <td>Liste MusikABC Verzeichnisse</td> </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>--list-files</strong></td> <td>Liste MusikABC Dateien</td> </tr>
</table>


<h2>ALLGEMEINE OPTIONEN <a href="#toc" name="general_options">&uarr;</a></h2>

<h3 id="export">Allgemeine Optionen</h3>

&nbsp;&nbsp; <a href="#synopsis"><strong>abc2html</strong></a> <strong>-X</strong>=<em>json|json+command|man|markdown</em> | <strong>-h</strong><em>[=synopsis|...]</em> | <strong>-V</strong> | <strong>-R</strong>=<em>gui|cli|web|...</em> [<strong>-v</strong>]

<table style='width:100%'>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-X</strong>|<strong>--export</strong>=<em>json|json+command|man|markdown</em></td> <td>Exportiere formatierten Text<br />Hole Benutzerschnittstelle als Text
  <table>
   <tr><td style='padding-left:0.5em'><strong>-X</strong> man</td><td># Handbuch : Unix Handbuchseite - Hole Unix Handbuchseite</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-X</strong> markdown</td><td># Markdown : Formatierter Text - Hole formatierten Text</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-X</strong> json</td><td># Json : GUI - Hole Oyjl Json Benutzerschnittstelle</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-X</strong> json+command</td><td># Json + Kommando : GUI + Kommando - Hole Oyjl Json Benutzerschnittstelle mit Kommando</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-X</strong> export</td><td># Export : Alle verfügbaren Daten - Erhalte Daten für Entwickler. Das Format kann mit dem oyjl-args Werkzeug umgewandelt werden.</td></tr>
  </table>
  </td>
 </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-h</strong>|<strong>--help</strong><em>[=synopsis|...]</em></td> <td>Zeige Hilfetext an<br />Zeige Benutzungsinformationen und Hinweise für das Werkzeug.
  <table>
   <tr><td style='padding-left:0.5em'><strong>-h</strong> -</td><td># Vollständige Hilfe : Zeige Hilfe für alle Gruppen</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-h</strong> synopsis</td><td># Übersicht : Liste Gruppen - Zeige alle Gruppen mit Syntax</td></tr>
  </table>
  </td>
 </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-V</strong>|<strong>--version</strong></td> <td>Version</td> </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-R</strong>|<strong>--render</strong>=<em>gui|cli|web|...</em></td> <td>Darstellung
  <table>
   <tr><td style='padding-left:0.5em'><strong>-R</strong> gui</td><td># Gui : Zeige UI - Zeige eine interaktive grafische Benutzerschnittstelle.</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-R</strong> cli</td><td># Cli : Zeige UI - Zeige Hilfstext für Benutzerschnittstelle auf der Kommandozeile.</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-R</strong> web</td><td># Web : Starte Web Server - Starte lokalen Web Service für die Darstellung in einem Webbrowser. Die -R=web;help Unteroption zeigt weitere Informationen an.</td></tr>
   <tr><td style='padding-left:0.5em'><strong>-R</strong> -</td>
  </table>
  </td>
 </tr>
 <tr><td style='padding-left:1em;padding-right:1em;vertical-align:top;width:25%'><strong>-v</strong>|<strong>--verbose</strong></td> <td>Mehr Infos</td> </tr>
</table>


<h2>BEISPIELE <a href="#toc" name="examples">&uarr;</a></h2>

#### Liste MusikABC Titel
&nbsp;&nbsp;abc2html -l -i=mein.abc
#### Liste MusikABC Titel und zeige T und R Kopffelder
&nbsp;&nbsp;abc2html -l=header=T,R -i=mein.abc
#### Liste MusikABC Dateien
&nbsp;&nbsp;abc2html --list-files -i=~/Downloads
#### Liste MusikABC Verzeichnisse
&nbsp;&nbsp;abc2html -P -i=~
#### Wandle MusikABC eingebettet in HTML nach ABC
&nbsp;&nbsp;abc2html -f=abc -i=mein.abc.html
#### Wandle MusikABC nach HTML
&nbsp;&nbsp;abc2html -f=html -i=mein.abc -o=mein.abc.html
#### Wandle MusikABC nach SVG Bild
&nbsp;&nbsp;abc2html -f=svg -i=mein.abc -F=select=[11] -o=mein.abc.svg
#### Wandle MusikABC nach PDF Dokument
&nbsp;&nbsp;abc2html -f=pdf -i=mein.abc -F=select=[11,"Titel",3] -o=mein.abc.pdf

<h2>SYNTAX <a href="#toc" name="syntax">&uarr;</a></h2>

#### F: datei-name.[png|svg|jpg]
&nbsp;&nbsp;Die drei Bildformate werden zusammen mit der --format=HTML Ausgabe berücksichtigt.
  <br />
&nbsp;&nbsp;Die F Kopfzeile ist wiederholbar.

<h2>SIEHE AUCH <a href="#toc" name="seealso">&uarr;</a></h2>

 ABC Musikformat

&nbsp;&nbsp;<a href="https://abcnotation.com">https://abcnotation.com</a>
&nbsp;&nbsp;[abcm2ps](abcm2ps.html)<a href="abcm2ps.md">(1)</a>

&nbsp;&nbsp;<a href="http://moinejf.free.fr/">http://moinejf.free.fr/</a>
 ABC_UI

&nbsp;&nbsp;<a href="http://dev.music.free.fr/index.html">http://dev.music.free.fr/index.html</a>
 Musyng Kite Soundfont

&nbsp;&nbsp;<a href="https://gleitz.github.io/midi-js-soundfonts/">https://gleitz.github.io/midi-js-soundfonts/</a>

<h2>AUTOR <a href="#toc" name="author">&uarr;</a></h2>

Kai-Uwe Behrmann ku.b@gmx.de

<h2>KOPIERRECHT <a href="#toc" name="copyright">&uarr;</a></h2>

*Copyright © 2016-2025 Kai-Uwe Behrmann*


<a name="license"></a>
### Lizenz
AGPL-3.0 <a href="https://opensource.org/licenses/AGPL-3.0">https://opensource.org/licenses/AGPL-3.0</a>

